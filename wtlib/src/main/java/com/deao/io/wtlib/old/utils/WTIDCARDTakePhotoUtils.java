package com.deao.io.wtlib.old.utils;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;

import com.deao.io.wtlib.R;
import com.deao.io.wtlib.old.LoggerUtils;
import com.deao.io.wtlib.old.com.CameraActivity;
import com.wintone.passport.sdk.model.People;
import com.wintone.passport.sdk.uitls.Devcode;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;


/**
 * Created by Administrator on 2016/7/19.
 * 拍照识别的工具类.
 * 使用方法:
 * 1.使用静态的初始化的方法进行初始化
 * 2.调用识别方法
 * 3.要在调用的方法中回调本类中的onActivityForResult方法. ※ important
 * 4.在调用的类中重写本类的类回调接口,可以获取到识别结束后的结果.
 *
 * 2016/8/17
 * 1.添加了fragment的支持,启动的时候使用fragment启动,回调中(onActivityForResult 直接回调当前类的同一个方法)
 * 2.识别的结果通过接口回调,传回,注意要在启动类中实现.
 * 3.※在回调onActivityForResult 这个方法的时候,Activity就调用有Activity参数的方法.Fragment就调用Fragment的方法,否则会造成无法回调到结果.
 * 4.添加了对拍照识别,调用系统的拍照,然后送识别的方法支持.
 */
public class WTIDCARDTakePhotoUtils {
    public final static int TAKE_PHOTO_IDCARD = 0x0011;
    public final static int RECO_PHOTO_IDCARD = 0x0012;
    public final static int DEVICE_TAKE_PHOTO_IDCARD = 0x0013;

    public static final String PATH = Environment.getExternalStorageDirectory()
            .toString() + "/AndroidWT";

    private String selectPath;
    private String sn = "";
    private boolean isCatchPreview = false;
    private boolean isCatchPicture = false;
    private int srcwidth;
    private int srcheight;
    private int WIDTH;
    private int HEIGHT;
    int nMainID = 0;
    int[] nSubID;
    private String[] type;
    private String[] recogTypes = new String[2];
    public static int DIALOG_ID = -1;
    public boolean isVINRecog = false;
    public int recogType = -1;// 1代表自动识别，2代表划框识别，3代表划线识别
    private final String facePath;
    private static WTIDCARDTakePhotoUtils wTIDCARDTakePhotoUtils;

    private WTIDCARDTakePhotoUtils() {
        checkCameraParameters();
        facePath = PATH + "/face";
        if (!new File(facePath).exists()) {
          new File(facePath).mkdirs();
        }
    }

    public static WTIDCARDTakePhotoUtils newinitlization() {
        if (wTIDCARDTakePhotoUtils == null) {
            synchronized (WTIDCARDTakePhotoUtils.class) {
                if (wTIDCARDTakePhotoUtils == null)
                    wTIDCARDTakePhotoUtils = new WTIDCARDTakePhotoUtils();
            }
        }
        return wTIDCARDTakePhotoUtils;
    }
    private void checkCameraParameters() {
        // 读取支持的预览尺寸
        Camera camera = null;
        try {
            camera = Camera.open();
            if (camera != null) {
                // 读取支持的预览尺寸,优先选择640后320
                Camera.Parameters parameters = camera.getParameters();
                List<Integer> SupportedPreviewFormats = parameters
                        .getSupportedPreviewFormats();
                for (int i = 0; i < SupportedPreviewFormats.size(); i++) {
                    System.out.println("PreviewFormats="
                            + SupportedPreviewFormats.get(i));
                }
                LoggerUtils.i(
                        "preview-size-values:"
                                + parameters.get("preview-size-values"));
                List<Camera.Size> previewSizes = splitSize(
                        parameters.get("preview-size-values"), camera);// parameters.getSupportedPreviewSizes();

                // 冒泡排序算法 实现分辨率从小到大排列
                // 该算法以分辨率的宽度为准，如果宽度相等，则判断高度
                int tempWidth = 0;
                int tempHeight = 0;
                for (int i = 0; i < previewSizes.size(); i++) {
                    for (int j = i + 1; j < previewSizes.size(); j++) {
                        if (previewSizes.get(i).width > previewSizes.get(j).width) {

                            tempWidth = previewSizes.get(i).width;
                            tempHeight = previewSizes.get(i).height;
                            previewSizes.get(i).width = previewSizes.get(j).width;
                            previewSizes.get(i).height = previewSizes.get(j).height;
                            previewSizes.get(j).width = tempWidth;
                            previewSizes.get(j).height = tempHeight;

                        } else if (previewSizes.get(i).width == previewSizes
                                .get(j).width
                                && previewSizes.get(i).height > previewSizes
                                .get(j).height) {
                            tempWidth = previewSizes.get(i).width;
                            tempHeight = previewSizes.get(i).height;
                            previewSizes.get(i).width = previewSizes.get(j).width;
                            previewSizes.get(i).height = previewSizes.get(j).height;
                            previewSizes.get(j).width = tempWidth;
                            previewSizes.get(j).height = tempHeight;
                        }
                    }
                }
                for (int i = 0; i < previewSizes.size(); i++) {
                    System.out.println("宽度:" + previewSizes.get(i).width + "--"
                            + "高度:" + previewSizes.get(i).height);
                }
                // 冒泡排序算法
                // 该段程序主要目的是为了遵循:优先选择比640*480大的并且是最接近的而且是比例为4:3的原则编写的。
                for (int i = 0; i < previewSizes.size(); i++) {
                    // 当预览宽度和高度分别大于640和480并且宽和高的比为4:3时。
                    if (previewSizes.get(i).width > 640
                            && previewSizes.get(i).height > 480
                            && (((float) previewSizes.get(i).width / previewSizes
                            .get(i).height) == (float) 4 / 3)) {
                        isCatchPreview = true;
                        WIDTH = previewSizes.get(i).width;
                        HEIGHT = previewSizes.get(i).height;
                        break;
                    }
                    // 如果在640*480前没有满足的值，WIDTH和HEIGHT就都为0，然后进行如下判断，看是否有640*480，如果有则赋值，如果没有则进行下一步验证。
                    if (previewSizes.get(i).width == 640
                            && previewSizes.get(i).height == 480 && WIDTH < 640
                            && HEIGHT < 480) {
                        isCatchPreview = true;
                        WIDTH = 640;
                        HEIGHT = 480;
                    }
                    if (previewSizes.get(i).width == 320
                            && previewSizes.get(i).height == 240 && WIDTH < 320
                            && HEIGHT < 240) {// 640 //480
                        isCatchPreview = true;
                        WIDTH = 320;
                        HEIGHT = 240;
                    }
                }
                LoggerUtils.i("isCatchPreview=" + isCatchPreview);

                // 读取支持的相机尺寸,优先选择1280后1600后2048
                List<Integer> SupportedPictureFormats = parameters
                        .getSupportedPictureFormats();
                for (int i = 0; i < SupportedPictureFormats.size(); i++) {
                    System.out.println("PictureFormats="
                            + SupportedPictureFormats.get(i));
                }
                LoggerUtils.i(
                        "picture-size-values:"
                                + parameters.get("picture-size-values"));
                List<Camera.Size> PictureSizes = splitSize(
                        parameters.get("picture-size-values"), camera);// parameters.getSupportedPictureSizes();
                for (int i = 0; i < PictureSizes.size(); i++) {
                    if (PictureSizes.get(i).width == 2048
                            && PictureSizes.get(i).height == 1536) {
                        // 优先选择小的照片分辨率
                        if ((srcwidth == 0 && srcheight == 0)
                                || (srcwidth > 2048 && srcheight > 1536)) {
                            isCatchPicture = true;
                            srcwidth = 2048;
                            srcheight = 1536;
                        }

                    }
                    if (PictureSizes.get(i).width == 1600
                            && PictureSizes.get(i).height == 1200) {
                        if ((srcwidth == 0 && srcheight == 0)
                                || (srcwidth > 1600 && srcheight > 1200)) {
                            isCatchPicture = true;
                            srcwidth = 1600;
                            srcheight = 1200;
                        }

                    }
                    if (PictureSizes.get(i).width == 1280
                            && PictureSizes.get(i).height == 960) {
                        if ((srcwidth == 0 && srcheight == 0)
                                || (srcwidth > 1280 && srcheight > 960)) {
                            isCatchPicture = true;
                            srcwidth = 1280;
                            srcheight = 960;
                        }
                    }
                }
                LoggerUtils.i("isCatchPicture=" + isCatchPicture);
            }
            camera.release();
            camera = null;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (camera != null) {
                try {
                    camera.release();
                    camera = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int readMainID() {
        int mainID = 0;
        String cfgPath = Environment.getExternalStorageDirectory().toString()
                + "/AndroidWT/idcard.cfg";
        File cfgFile = new File(cfgPath);
        char[] buf = new char[14];
        if (!cfgFile.exists()) {
            return 0;
        } else {
            try {
                FileReader fr = new FileReader(cfgFile);
                fr.read(buf);
                String str = String.valueOf(buf);
                String[] splits = str.split("==##");
                mainID = Integer.valueOf(splits[0]);
                LoggerUtils.i( "readMainID mainID=" + mainID);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mainID;
    }

    private ArrayList<Camera.Size> splitSize(String str, Camera camera) {
        if (str == null)
            return null;
        StringTokenizer tokenizer = new StringTokenizer(str, ",");
        ArrayList<Camera.Size> sizeList = new ArrayList<Camera.Size>();
        while (tokenizer.hasMoreElements()) {
            Camera.Size size = strToSize(tokenizer.nextToken(), camera);
            if (size != null)
                sizeList.add(size);
        }
        if (sizeList.size() == 0)
            return null;
        return sizeList;
    }

    private Camera.Size strToSize(String str, Camera camera) {
        if (str == null)
            return null;
        int pos = str.indexOf('x');
        if (pos != -1) {
            String width = str.substring(0, pos);
            String height = str.substring(pos + 1);
            return camera.new Size(Integer.parseInt(width),
                    Integer.parseInt(height));
        }
        return null;
    }

    public void startPhotoRecoSFZ(Activity activity) {
        nMainID = 2;
        if (isCatchPreview == true && isCatchPicture == true) {
            Intent intent = new Intent();
            intent.setClass(activity, CameraActivity.class);
            LoggerUtils.i( "拍摄分辨率为: " + srcwidth + " * " + srcheight);
            LoggerUtils.i( "预览分辨率为: " + WIDTH + " * " + HEIGHT);
            writePreferences(activity, "", "WIDTH", WIDTH);
            writePreferences(activity, "", "HEIGHT", HEIGHT);
            writePreferences(activity, "", "srcwidth", srcwidth);
            writePreferences(activity, "", "srcheight", srcheight);
            writePreferences(activity, "", "isAutoTakePic", 0);
            intent.putExtra("WIDTH", WIDTH);
            intent.putExtra("HEIGHT", HEIGHT);
            intent.putExtra("srcwidth", srcwidth);
            intent.putExtra("srcheight", srcheight);
            intent.putExtra("nMainID", nMainID);

            activity.startActivityForResult(intent, TAKE_PHOTO_IDCARD);
            activity.overridePendingTransition(R.anim.zoomin, R.anim.zoomout);

        } else {
            String partpath = Environment
                    .getExternalStorageDirectory() + "/wtimage";
            File dir = new File(partpath);
            if (!dir.exists()) {
                dir.mkdir();
            }
            Date date = new Date();
            selectPath = partpath + "/idcard" + date.getTime()
                    + ".jpg";
            Intent takePictureFromCameraIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureFromCameraIntent.putExtra(
                    MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(selectPath)));
            activity.startActivityForResult(takePictureFromCameraIntent, DEVICE_TAKE_PHOTO_IDCARD);
            activity.overridePendingTransition(R.anim.zoomin, R.anim.zoomout);
        }
    }


    public void startPhotoRecoSFZ(Fragment fragment) {
        nMainID = 2;
        if (isCatchPreview == true && isCatchPicture == true ) {
            Intent intent = new Intent();
            intent.setClass(fragment.getActivity(), CameraActivity.class);
            LoggerUtils.i( "拍摄分辨率为: " + srcwidth + " * " + srcheight);
            LoggerUtils.i( "预览分辨率为: " + WIDTH + " * " + HEIGHT);
            writePreferences(fragment.getActivity(), "", "WIDTH", WIDTH);
            writePreferences(fragment.getActivity(), "", "HEIGHT", HEIGHT);
            writePreferences(fragment.getActivity(), "", "srcwidth", srcwidth);
            writePreferences(fragment.getActivity(), "", "srcheight", srcheight);
            writePreferences(fragment.getActivity(), "", "isAutoTakePic", 0);
            intent.putExtra("WIDTH", WIDTH);
            intent.putExtra("HEIGHT", HEIGHT);
            intent.putExtra("srcwidth", srcwidth);
            intent.putExtra("srcheight", srcheight);
            intent.putExtra("nMainID", nMainID);

            fragment.startActivityForResult(intent, TAKE_PHOTO_IDCARD);
            fragment.getActivity().overridePendingTransition(R.anim.zoomin, R.anim.zoomout);

        } else {
            String partpath = Environment
                    .getExternalStorageDirectory() + "/wtimage";
            File dir = new File(partpath);
            if (!dir.exists()) {
                dir.mkdir();
            }
            Date date = new Date();
            selectPath = partpath + "/idcard" + date.getTime()
                    + ".jpg";
            Intent takePictureFromCameraIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureFromCameraIntent.putExtra(
                    MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(selectPath)));
            fragment.startActivityForResult(takePictureFromCameraIntent, DEVICE_TAKE_PHOTO_IDCARD);
            fragment.getActivity().overridePendingTransition(R.anim.zoomin, R.anim.zoomout);
        }
    }


    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        if (TAKE_PHOTO_IDCARD == requestCode && Activity.RESULT_OK == resultCode) {
            String sfzPicture = data.getStringExtra("path");
            int nMainID = data.getIntExtra("nMainID", 2);

            boolean cut = data.getBooleanExtra("cut", true);

            // 发送识别
            String logopath = "";

            Intent intent = new Intent("wintone.idcard");
            Bundle bundle = new Bundle();
            int nSubID[] = null;// {0x0001};

            bundle.putInt("nTypeInitIDCard", 0); // 保留，传0即可
            bundle.putString("lpFileName", sfzPicture);// 指定的图像路径
            bundle.putInt("nTypeLoadImageToMemory", 0);// 0不确定是哪种图像，1可见光图，2红外光图，4紫外光图
            bundle.putString("lpHeadFileName", facePath + "/face.jpg");
            bundle.putInt("nMainID", nMainID); // 证件的主类型。6是行驶证，2是二代证，这里只可以传一种证件主类型。每种证件都有一个唯一的ID号，可取值见证件主类型说明
            bundle.putIntArray("nSubID", nSubID); // 保存要识别的证件的子ID，每个证件下面包含的子类型见证件子类型说明。nSubID[0]=null，表示设置主类型为nMainID的所有证件。
            bundle.putBoolean("GetVersionInfo", true); //获取开发包的版本信息
            bundle.putString("devcode", Devcode.devcode);
            bundle.putString("logo", logopath); // logo路径，logo显示在识别等待页面右上角
            bundle.putBoolean("isCut", cut); // 如不设置此项默认自动裁切
            bundle.putString("returntype", "withvalue");// 返回值传递方式withvalue带参数的传值方式（新传值方式）
            intent.putExtras(bundle);
            activity.startActivityForResult(intent, RECO_PHOTO_IDCARD);
        } else if (DEVICE_TAKE_PHOTO_IDCARD == requestCode && Activity.RESULT_OK == resultCode){
            // 使用设备自带拍照,
            int nMainID = 2;

            boolean cut = true;

            // 发送识别
            String logopath = "";

            Intent intent = new Intent("wintone.idcard");
            Bundle bundle = new Bundle();
            int nSubID[] = null;// {0x0001};

            bundle.putInt("nTypeInitIDCard", 0); // 保留，传0即可
            bundle.putString("lpFileName", selectPath);// 指定的图像路径
            bundle.putInt("nTypeLoadImageToMemory", 0);// 0不确定是哪种图像，1可见光图，2红外光图，4紫外光图
            bundle.putString("lpHeadFileName", facePath + "/face.jpg");
            bundle.putInt("nMainID", nMainID); // 证件的主类型。6是行驶证，2是二代证，这里只可以传一种证件主类型。每种证件都有一个唯一的ID号，可取值见证件主类型说明
            bundle.putIntArray("nSubID", nSubID); // 保存要识别的证件的子ID，每个证件下面包含的子类型见证件子类型说明。nSubID[0]=null，表示设置主类型为nMainID的所有证件。
            bundle.putBoolean("GetVersionInfo", true); //获取开发包的版本信息
            bundle.putString("devcode", Devcode.devcode);
            bundle.putString("logo", logopath); // logo路径，logo显示在识别等待页面右上角
            bundle.putBoolean("isCut", cut); // 如不设置此项默认自动裁切
            bundle.putString("returntype", "withvalue");// 返回值传递方式withvalue带参数的传值方式（新传值方式）
            intent.putExtras(bundle);
            activity.startActivityForResult(intent, RECO_PHOTO_IDCARD);
        }else if (RECO_PHOTO_IDCARD == requestCode && Activity.RESULT_OK == resultCode) {
            OnRecoIDCardListener OnRecoIDCardListener = null;
            if (activity instanceof OnRecoIDCardListener) {
                OnRecoIDCardListener = (OnRecoIDCardListener) activity;
            }
            // 读识别返回值
            int ReturnAuthority = data.getIntExtra("ReturnAuthority", -100000);// 取激活状态
            System.out.println("ReturnAuthority:" + ReturnAuthority + "---"
                    + "nMainID:" + nMainID);
            int ReturnInitIDCard = data
                    .getIntExtra("ReturnInitIDCard", -100000);// 取初始化返回值
            int ReturnLoadImageToMemory = data.getIntExtra(
                    "ReturnLoadImageToMemory", -100000);// 取读图像的返回值
            int ReturnRecogIDCard = data.getIntExtra("ReturnRecogIDCard",
                    -100000);// 取识别的返回值
            System.out.println("识别核心版本号:" +
                    data.getStringExtra("ReturnGetVersionInfo"));
            // //取版本信息，如果定义了GetVersionInfo为true才能取版本信息
            // System.out.println("ReturnGetSubID" +
            // intentget.getIntExtra("ReturnGetSubID",-1));
            // //取识别图像的子类型id，如果定义了GetSubID 为true才能取识别图像的子类型id
            // System.out.println("" +
            // intentget.getIntExtra("ReturnSaveHeadImage",-1));
            // //取SaveHeadImage返回值，如果定义了lpHeadFileName的路径才能得到头像
            // System.out.println("ReturnUserData:" +
            // intentget.getStringExtra("ReturnUserData"));
            LoggerUtils.i( "ReturnLPFileName:"
                            + data.getStringExtra("ReturnLPFileName"));

            if (ReturnAuthority == 0 && ReturnInitIDCard == 0
                    && ReturnLoadImageToMemory == 0 && ReturnRecogIDCard > 0) {
                // System.out.println("接收结果");
                String result = "";
                String[] fieldname = (String[]) data
                        .getSerializableExtra("GetFieldName");
                String[] fieldvalue = (String[]) data
                        .getSerializableExtra("GetRecogResult");
                String time = data.getStringExtra("ReturnTime");
                String ReturnLPFileName = data.getStringExtra("ReturnLPFileName");

                // 新建一个身份证信息对象
                People people = new People();
                if (null != fieldname) {
                    int count = fieldname.length;
                    for (int i = 0; i < count; i++) {
                        if (fieldname[i] == null) {
                            break;
                        }

                        if (nMainID == 2) { // 身份证识别
                            switch (fieldname[i]) {
                                case "姓名":
                                    people.setPeopleName(fieldvalue[i]);
                                    break;
                                case "公民身份号码":
                                    people.setPeopleIDCode(fieldvalue[i]);
                                    break;
                                case "出生":
                                    people.setPeopleBirthday(fieldvalue[i]);
                                    break;
                                case "住址":
                                    people.setPeopleAddress(fieldvalue[i]);
                                    break;
                                case "性别":
                                    people.setPeopleSex(fieldvalue[i]);
                                    break;
                                case "民族":
                                    people.setPeopleNation(fieldvalue[i]);
                                    break;
                            }
                        } else if (nMainID == 5) { // 驾照识别结果封装
                            switch (fieldname[i]) {
                                case "姓名":
                                    people.setPeopleName(fieldvalue[i]);
                                    break;
                                case "证号":
                                    people.setPeopleIDCode(fieldvalue[i]);
                                    break;
                                case "出生日期":
                                    people.setPeopleBirthday(fieldvalue[i]);
                                    break;
                                case "住址":
                                    people.setPeopleAddress(fieldvalue[i]);
                                    break;
                                case "性别":
                                    people.setPeopleSex(fieldvalue[i]);
                                    break;
                                case "民族":
                                    people.setPeopleNation(fieldvalue[i]);
                                    break;
                            }
                        }
                    }

                    // 把图片解析成byte数组
                    if (new File(facePath + "/face.jpg").exists()) {
                        byte[] bytes = getBytesFromFile(facePath + "/face.jpg");
                        people.setPhoto(bytes);
                    }

                    if (selectPath != null) {
                        people.setIDCardPhoto(selectPath);
                    }
                    if (OnRecoIDCardListener != null)
                        OnRecoIDCardListener.onSucessRecoIDCARD(people);
                    LoggerUtils.i(people.toString());
                    activity = null; // 识别结束,便于释放内存

                } else {
                    String str = "";
                    if (ReturnAuthority == -100000) {
                        str = activity.getString(R.string.exception) + ReturnAuthority;
                    } else if (ReturnAuthority != 0) {
                        str = activity.getString(R.string.exception1) + ReturnAuthority;
                    } else if (ReturnInitIDCard != 0) {
                        str = activity.getString(R.string.exception2) + ReturnInitIDCard;
                    } else if (ReturnLoadImageToMemory != 0) {
                        if (ReturnLoadImageToMemory == 3) {
                            str = activity.getString(R.string.exception3)
                                    + ReturnLoadImageToMemory;
                        } else if (ReturnLoadImageToMemory == 1) {
                            str = activity.getString(R.string.exception4)
                                    + ReturnLoadImageToMemory;
                        } else {
                            str = activity.getString(R.string.exception5)
                                    + ReturnLoadImageToMemory;
                        }
                    } else if (ReturnRecogIDCard != 0) {
                        str = activity.getString(R.string.exception6) + ReturnRecogIDCard;
                    }
                    Toast.makeText(activity, str, Toast.LENGTH_SHORT).show();

                    LoggerUtils.i(str);
                }

            }
        }
    }

    public void onActivityResult(Fragment fragment, int requestCode, int resultCode, Intent data) {
        if (TAKE_PHOTO_IDCARD == requestCode && Activity.RESULT_OK == resultCode) {
            String sfzPicture = data.getStringExtra("path");
            int nMainID = data.getIntExtra("nMainID", 2);

            boolean cut = data.getBooleanExtra("cut", true);

            // 发送识别
            String logopath = "";

            Intent intent = new Intent("wintone.idcard");
            Bundle bundle = new Bundle();
            int nSubID[] = null;// {0x0001};

            bundle.putInt("nTypeInitIDCard", 0); // 保留，传0即可
            bundle.putString("lpFileName", sfzPicture);// 指定的图像路径
            bundle.putInt("nTypeLoadImageToMemory", 0);// 0不确定是哪种图像，1可见光图，2红外光图，4紫外光图
            bundle.putString("lpHeadFileName", facePath + "/face.jpg");
            bundle.putInt("nMainID", nMainID); // 证件的主类型。6是行驶证，2是二代证，这里只可以传一种证件主类型。每种证件都有一个唯一的ID号，可取值见证件主类型说明
            bundle.putIntArray("nSubID", nSubID); // 保存要识别的证件的子ID，每个证件下面包含的子类型见证件子类型说明。nSubID[0]=null，表示设置主类型为nMainID的所有证件。
            bundle.putBoolean("GetVersionInfo", true); //获取开发包的版本信息
            bundle.putString("devcode", Devcode.devcode);
            bundle.putString("logo", logopath); // logo路径，logo显示在识别等待页面右上角
            bundle.putBoolean("isCut", cut); // 如不设置此项默认自动裁切
            bundle.putString("returntype", "withvalue");// 返回值传递方式withvalue带参数的传值方式（新传值方式）
            intent.putExtras(bundle);
            fragment.startActivityForResult(intent, RECO_PHOTO_IDCARD);
        } else if (DEVICE_TAKE_PHOTO_IDCARD == requestCode && Activity.RESULT_OK == resultCode){
            // 使用设备自带拍照,
            int nMainID = 2;

            boolean cut = true;

            // 发送识别
            String logopath = "";

            Intent intent = new Intent("wintone.idcard");
            Bundle bundle = new Bundle();
            int nSubID[] = null;// {0x0001};

            bundle.putInt("nTypeInitIDCard", 0); // 保留，传0即可
            bundle.putString("lpFileName", selectPath);// 指定的图像路径
            bundle.putInt("nTypeLoadImageToMemory", 0);// 0不确定是哪种图像，1可见光图，2红外光图，4紫外光图
            bundle.putString("lpHeadFileName", facePath + "/face.jpg");
            bundle.putInt("nMainID", nMainID); // 证件的主类型。6是行驶证，2是二代证，这里只可以传一种证件主类型。每种证件都有一个唯一的ID号，可取值见证件主类型说明
            bundle.putIntArray("nSubID", nSubID); // 保存要识别的证件的子ID，每个证件下面包含的子类型见证件子类型说明。nSubID[0]=null，表示设置主类型为nMainID的所有证件。
            bundle.putBoolean("GetVersionInfo", true); //获取开发包的版本信息
            bundle.putString("devcode", Devcode.devcode);
            bundle.putString("logo", logopath); // logo路径，logo显示在识别等待页面右上角
            bundle.putBoolean("isCut", cut); // 如不设置此项默认自动裁切
            bundle.putString("returntype", "withvalue");// 返回值传递方式withvalue带参数的传值方式（新传值方式）
            intent.putExtras(bundle);
            fragment.startActivityForResult(intent, RECO_PHOTO_IDCARD);
        } else if (RECO_PHOTO_IDCARD == requestCode && Activity.RESULT_OK == resultCode) {
            OnRecoIDCardListener OnRecoIDCardListener = null;
            if (fragment instanceof OnRecoIDCardListener) {
                OnRecoIDCardListener = (OnRecoIDCardListener) fragment;
            }
            // 读识别返回值
            int ReturnAuthority = data.getIntExtra("ReturnAuthority", -100000);// 取激活状态
            System.out.println("ReturnAuthority:" + ReturnAuthority + "---"
                    + "nMainID:" + nMainID);
            int ReturnInitIDCard = data
                    .getIntExtra("ReturnInitIDCard", -100000);// 取初始化返回值
            int ReturnLoadImageToMemory = data.getIntExtra(
                    "ReturnLoadImageToMemory", -100000);// 取读图像的返回值
            int ReturnRecogIDCard = data.getIntExtra("ReturnRecogIDCard",
                    -100000);// 取识别的返回值
            System.out.println("识别核心版本号:" +
                    data.getStringExtra("ReturnGetVersionInfo"));
            // //取版本信息，如果定义了GetVersionInfo为true才能取版本信息
            // System.out.println("ReturnGetSubID" +
            // intentget.getIntExtra("ReturnGetSubID",-1));
            // //取识别图像的子类型id，如果定义了GetSubID 为true才能取识别图像的子类型id
            // System.out.println("" +
            // intentget.getIntExtra("ReturnSaveHeadImage",-1));
            // //取SaveHeadImage返回值，如果定义了lpHeadFileName的路径才能得到头像
            // System.out.println("ReturnUserData:" +
            // intentget.getStringExtra("ReturnUserData"));
            LoggerUtils.i( "ReturnLPFileName:"
                    + data.getStringExtra("ReturnLPFileName"));

            if (ReturnAuthority == 0 && ReturnInitIDCard == 0
                    && ReturnLoadImageToMemory == 0 && ReturnRecogIDCard > 0) {
                // System.out.println("接收结果");
                String result = "";
                String[] fieldname = (String[]) data
                        .getSerializableExtra("GetFieldName");
                String[] fieldvalue = (String[]) data
                        .getSerializableExtra("GetRecogResult");
                String time = data.getStringExtra("ReturnTime");
                String ReturnLPFileName = data.getStringExtra("ReturnLPFileName");

                // 新建一个身份证信息对象
                People people = new People();
                if (null != fieldname) {
                    int count = fieldname.length;
                    for (int i = 0; i < count; i++) {
                        if (fieldname[i] == null) {
                            break;
                        }

                        if (nMainID == 2) { // 身份证识别
                            switch (fieldname[i]) {
                                case "姓名":
                                    people.setPeopleName(fieldvalue[i]);
                                    break;
                                case "公民身份号码":
                                    people.setPeopleIDCode(fieldvalue[i]);
                                    break;
                                case "出生":
                                    people.setPeopleBirthday(fieldvalue[i]);
                                    break;
                                case "住址":
                                    people.setPeopleAddress(fieldvalue[i]);
                                    break;
                                case "性别":
                                    people.setPeopleSex(fieldvalue[i]);
                                    break;
                                case "民族":
                                    people.setPeopleNation(fieldvalue[i]);
                                    break;
                            }
                        } else if (nMainID == 5) { // 驾照识别结果封装
                            switch (fieldname[i]) {
                                case "姓名":
                                    people.setPeopleName(fieldvalue[i]);
                                    break;
                                case "证号":
                                    people.setPeopleIDCode(fieldvalue[i]);
                                    break;
                                case "出生日期":
                                    people.setPeopleBirthday(fieldvalue[i]);
                                    break;
                                case "住址":
                                    people.setPeopleAddress(fieldvalue[i]);
                                    break;
                                case "性别":
                                    people.setPeopleSex(fieldvalue[i]);
                                    break;
                                case "民族":
                                    people.setPeopleNation(fieldvalue[i]);
                                    break;
                            }
                        }
                    }

                    // 把图片解析成byte数组
                    if (new File(facePath + "/face.jpg").exists()) {
                        byte[] bytes = getBytesFromFile(facePath + "/face.jpg");
                        people.setPhoto(bytes);
                    }

                    if (selectPath != null) {
                        people.setIDCardPhoto(selectPath);
                    }
                    if (OnRecoIDCardListener != null)
                        OnRecoIDCardListener.onSucessRecoIDCARD(people);
                    LoggerUtils.i(people.toString());
                    fragment = null; // 识别结束,便于释放内存

                } else {
                    String str = "";
                    if (ReturnAuthority == -100000) {
                        str = fragment.getString(R.string.exception) + ReturnAuthority;
                    } else if (ReturnAuthority != 0) {
                        str = fragment.getString(R.string.exception1) + ReturnAuthority;
                    } else if (ReturnInitIDCard != 0) {
                        str = fragment.getString(R.string.exception2) + ReturnInitIDCard;
                    } else if (ReturnLoadImageToMemory != 0) {
                        if (ReturnLoadImageToMemory == 3) {
                            str = fragment.getString(R.string.exception3)
                                    + ReturnLoadImageToMemory;
                        } else if (ReturnLoadImageToMemory == 1) {
                            str = fragment.getString(R.string.exception4)
                                    + ReturnLoadImageToMemory;
                        } else {
                            str = fragment.getString(R.string.exception5)
                                    + ReturnLoadImageToMemory;
                        }
                    } else if (ReturnRecogIDCard != 0) {
                        str = fragment.getString(R.string.exception6) + ReturnRecogIDCard;
                    }
                    Toast.makeText(fragment.getActivity(), str, Toast.LENGTH_SHORT).show();

                    LoggerUtils.i(str);
                }

            }
        }
    }
    protected void writePreferences(Context context, String perferencesName, String key,
                                    int value) {
        SharedPreferences preferences = context.getSharedPreferences(perferencesName,
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * 获得指定文件的byte数组
     */

    public static byte[] getBytesFromFile(String filePath) {
        byte[] buffer = null;
        try {
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1000];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }

    public interface OnRecoIDCardListener {
        // 身份证识别成功回调
        public void onSucessRecoIDCARD(People people);
    }

}
