package com.wintone.passport.sdk.model;

/**
 * Created by Administrator on 2016/6/13.
 * 常量封装
 */
public class Constants {
    public static final String RESULT_RECO_INTTENT_KEY = "wt_people";

    public static final String RESULT_RECO_ERROR_CODE = "wt_error_code";

    public static final String RESULT_RECO_ERROR_CONTENT = "wt_error_content";

    // 车牌拍照的路径
    public static final String PLATE_PAOTO = "plate_photo_path";


}
