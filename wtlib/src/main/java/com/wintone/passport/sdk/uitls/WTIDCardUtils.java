package com.wintone.passport.sdk.uitls;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import com.deao.io.wtlib.old.LoggerUtils;
import com.wintone.passport.sdk.model.Constants;
import com.wintone.passport.sdk.model.People;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import wintone.idcard.android.RecogService;


/**
 * 文通身份证识别的工具类封装
 * 调用的扫描识别/系统拍照 送识别
 * 使用方法:
 * 1.newinitlization 静态方法初始化对象
 * 2.调用startRecoSFZ 启动身份证识别模块
 * 3.在调用的类中重写OnActivityForResult并在里面调用本类方法onActivityForResult
 * 4.在启动之前实现onSucessReco这个接口中的方法,然后可以在识别成功之后回调.返回的是当前类封装的people对象
 * ps:只封装了在Fragment中调用,Activity中的调用并没有封装;主要区别在Activity/Fragment的StartActivityForResult的差异.
 * 1.后期已经添加了Activity的回调
 * 2.添加了识别身份证的回调
 */
public class WTIDCardUtils {

    private static final int WT_IDCARD_PHOTO = 0x001001;
    private static final int IDCARD_RECO_RESULT = 0x001002;
    private static final int IDCARD_USELOCAL_PHOTO = 0x001003;
    private static final int DRIVERCARD_PHOTO = 0x001004;
    private static final int WT_DRIVERCARD_PHOTO = 0x001005;
    private static final int DRIVER_RECO = 0x001006;
    public static final int RECO_SCAN_IDCARD_REQUESTCODE_RESULT = 0x0001999;
    public static final int RECO_SCAN_DRIVER_REQUESTCODE_RESULT = 0x0001998;
    // 保存文通的内容
    public static String WTsdDir;
	public static Camera mCamera;
    private static Context context;
    private int srcwidth;
    private int srcheight;
    private int WIDTH;
    private int HEIGHT;
    private String selectPath;
    private final String facePhoto = WTsdDir + "/face";
    private boolean sucessReco = false;
    private boolean isCatchPreview = false;
    private boolean isCatchPicture = false;
    private int nMainID;
    private OnRecoIDCARDListener recoLIDCARDistener;
    private onRecoDriverListener recoDriverListener;
    private static WTIDCardUtils wtidCardUtils;
    private Uri takePhotoFile;


    private WTIDCardUtils() {
        //找到内存卡,并且创建一个文件.

//        WTsdDir = Environment.getExternalStorageDirectory().getAbsolutePath();

        String storageState = Environment.getExternalStorageState();
        if (!Environment.MEDIA_REMOVED.equals(storageState)) {
            File mediaStorageDir = new File(
                    Environment.getExternalStorageDirectory(), "wtimage");
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {

                }
            }
        }

    }

    public static WTIDCardUtils newinitlization(Context contexts){
        context = contexts;

        if (wtidCardUtils == null)
            wtidCardUtils = new WTIDCardUtils();

        return wtidCardUtils;
    }



    /**
     * 文通使用身份证识别,使用fragment调用
     */
    public void startRecoSFZ(Fragment fragment) {

        if (fragment instanceof OnRecoIDCARDListener) {
            recoLIDCARDistener = (OnRecoIDCARDListener)fragment;
        }
        LoggerUtils.i("isCatchPreview=" + isCatchPreview
                + " isCatchPicture=" + isCatchPicture);
        // 如果刚开始不选择证件类型选项就会报-5异常，所以这里默认为1033
        nMainID = 2; // 写死为身份证类型

        if ("Coolpad".equals(android.os.Build.BRAND)) {
            //酷派手机使用拍照识别
            recoPhotoInCurrentClass(fragment, nMainID);
        } else {
            recoSFZDRIVERScanInCurrenClass(fragment, nMainID);
        }

    }



    public void startRecoSFZ(Activity activity) {
        if (activity instanceof OnRecoIDCARDListener) {
            recoLIDCARDistener = (OnRecoIDCARDListener)activity;

        }
        // writePreferences("Button", "Height", mbutcap.getHeight());
        LoggerUtils.i("isCatchPreview=" + isCatchPreview
                + " isCatchPicture=" + isCatchPicture);
        // 如果刚开始不选择证件类型选项就会报-5异常，所以这里默认为1033
        nMainID = 2; // 写死为身份证类型

        // 身份证识别的 nMainId 就是2
        if ("Coolpad".equals(android.os.Build.BRAND)) {
            //酷派手机使用拍照识别

            recoPhotoInCurrentClass(activity, nMainID);
        } else {
            recoSFZDRIVERScanInCurrenClass(activity, nMainID);
        }

    }

    /**
     * 驾照识别
     * @param fragment
     */
    public void startRecoDriverCard(Fragment fragment){
        if (fragment instanceof onRecoDriverListener) {
            recoDriverListener = (onRecoDriverListener)fragment;

        }
        nMainID = 5;
        // 身份证识别的 nMainId 就是2
        if ("Coolpad".equals(android.os.Build.BRAND)) {
            //酷派手机使用拍照识别
            recoPhotoInCurrentClass(fragment, nMainID);
        } else {
            recoSFZDRIVERScanInCurrenClass(fragment, nMainID);
        }
    }

    /**
     * 驾照识别
     * @param activity
     */
    public void startRecoDriverCard(Activity activity){
        if (activity instanceof onRecoDriverListener) {
            recoDriverListener = (onRecoDriverListener)activity;
        }
        nMainID = 5;
        // 身份证识别的 nMainId 就是2
        // 身份证识别的 nMainId 就是2
        if ("Coolpad".equals(android.os.Build.BRAND)) {
            //酷派手机使用拍照识别
            recoPhotoInCurrentClass(activity, nMainID);
        } else {
            recoSFZDRIVERScanInCurrenClass(activity, nMainID);
        }
    }
    /**
     * 识别代码好多重复的,这里直接写好相同的.
     * 这个方法是调用扫描识别证件
     * @param fragment 参数只能是Fragment或者Activity
     */
    private <T>void recoSFZDRIVERScanInCurrenClass(T fragment, int nMainId) {
        if (fragment instanceof Fragment) {
            // 身份证识别的 nMainId 就是2
            Intent intent = new Intent(context, com.wintone.passport.sdk.thread.CameraActivity.class);
            intent.putExtra("nMainId", nMainId);
            intent.putExtra("devcode", Devcode.devcode);
            intent.putExtra("flag", 0);
            ((Fragment)fragment).getActivity().startActivityForResult(intent, RECO_SCAN_IDCARD_REQUESTCODE_RESULT);
        } else if (fragment instanceof Activity) {
            // 身份证识别的 nMainId 就是2
            Intent intent = new Intent(context, com.wintone.passport.sdk.thread.CameraActivity.class);
            intent.putExtra("nMainId", nMainId);
            intent.putExtra("devcode", Devcode.devcode);
            intent.putExtra("flag", 0);
            ((Activity)fragment).startActivityForResult(intent, RECO_SCAN_IDCARD_REQUESTCODE_RESULT);
        }
    }

    /**
     * 这里使用拍照方式识别.部分低端手机 这里是拍照进入识别类.都从这个方法里面跳转
     * @param fragment
     * @param nMainId
     * @param <T>
     */
    private <T>void recoPhotoInCurrentClass(T fragment, int nMainId) {
        String wtimage = new File(Environment.getExternalStorageDirectory(), "wtimage").getAbsolutePath();
        // 身份证识别的 nMainId 就是2
        if (fragment instanceof Fragment) {
            // 调用系统拍照
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                    .format(new Date());
            takePhotoFile = Uri.fromFile(new File(wtimage + File.separator + "IMG_" + timeStamp + ".jpg"));
            // 拍照的文件存储在这里
            intent.putExtra(MediaStore.EXTRA_OUTPUT, takePhotoFile);
            ((Fragment)fragment).getActivity().startActivityForResult(intent, IDCARD_USELOCAL_PHOTO);
//            ((Fragment)fragment).getActivity().overridePendingTransition(R.anim.zoomin, R.anim.zoomout);


        } else if (fragment instanceof Activity) {

            // 调用系统拍照
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            // 拍照的文件存储在这里
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                    .format(new Date());
            takePhotoFile = Uri.fromFile(new File(wtimage + File.separator + "IMG_" + timeStamp + ".jpg"));

            intent.putExtra(MediaStore.EXTRA_OUTPUT, takePhotoFile);
            ((Activity)fragment).startActivityForResult(intent, IDCARD_USELOCAL_PHOTO);
//            ((Activity)fragment).overridePendingTransition(R.anim.zoomin, R.anim.zoomout);

        }
    }
    public void onActivityForResult(Activity activity,int requestCode, int resultCode, Intent data) {

        if (resultCode == activity.RESULT_OK && requestCode == RECO_SCAN_IDCARD_REQUESTCODE_RESULT) {
            // 扫描识别
            // 身份证识别的返回结果
            nMainID = 2;

            if (data.getStringExtra(Constants.RESULT_RECO_ERROR_CODE) != null) { //
                // 有识别的错误代码说明识别失败
                recoLIDCARDistener.onFailedReco();
            } else {
                if ((People) data.getParcelableExtra(Constants.RESULT_RECO_INTTENT_KEY) != null)
                    if (recoLIDCARDistener != null)
                        recoLIDCARDistener.onSucessReco((People) data.getParcelableExtra(Constants.RESULT_RECO_INTTENT_KEY));
            }

        }
        else if (resultCode == activity.RESULT_OK && requestCode == RECO_SCAN_DRIVER_REQUESTCODE_RESULT) {
            // 扫描识别
            // 驾照识别的返回结果
            nMainID = 5;

            if (data.getStringExtra(Constants.RESULT_RECO_ERROR_CODE) != null) { //
                // 有识别的错误代码说明识别失败
                if (recoLIDCARDistener != null)
                    recoLIDCARDistener.onFailedReco();
            } else {
                People people = (People) data.getParcelableExtra(Constants.RESULT_RECO_INTTENT_KEY);
                if (people != null && recoDriverListener != null)
                    recoDriverListener.onSucessRecoDriver(people);

            }
        } else if (resultCode == activity.RESULT_OK && requestCode == IDCARD_USELOCAL_PHOTO) {
            // 调用系统拍照的回调,这里是获取拍照的照片.然后调用文通的识别接口.

            nMainID = 2;
            if (data == null) {
                RecogService.isRecogByPath = true;

                selectPath = UriUtils.getUriRealPath(context, takePhotoFile);
            } else {
                RecogService.isRecogByPath = true;
                selectPath = data.getData().getPath();
            }

            // 这里再调用识别的类,进行识别调用
            Intent intent = new Intent("wintone.idcard");
            Bundle bundle = new Bundle();
            int nSubID[] = null;// {0x0001};
            bundle.putString("lpHeadFileName", facePhoto + "/face.jpg"); // 是否需要得到头像图片，如果需要请设置保存路径名，后缀只能为jpg、bmp、tif
            bundle.putBoolean("isGetRecogFieldPos", false);// 是否获取识别字段在原图的位置信息，默认为假，不获取
            // 必须在核心裁好的图上裁切才行
            bundle.putString("cls", "checkauto.com.IdcardRunner");
            bundle.putInt("nTypeInitIDCard", 0); // 保留，传0即可
            bundle.putString("lpFileName", selectPath);// 指定的图像路径
            bundle.putInt("nTypeLoadImageToMemory", 0);// 0不确定是哪种图像，1可见光图，2红外光图，4紫外光图
            bundle.putString("cutSavePath", "");// 裁切图片的存储路径
            bundle.putInt("nMainID", nMainID); // 证件的主类型。6是行驶证，2是二代证，这里只可以传一种证件主类型。每种证件都有一个唯一的ID号，可取值见证件主类型说明
            bundle.putIntArray("nSubID", nSubID); // 保存要识别的证件的子ID，每个证件下面包含的子类型见证件子类型说明。nSubID[0]=null，表示设置主类型为nMainID的所有证件。

            bundle.putBoolean("GetVersionInfo", true); //获取开发包的版本信息
            bundle.putString("devcode", Devcode.devcode);

            bundle.putBoolean("isCut", false); // 如不设置此项默认自动裁切,false 关闭自动切割问题
            bundle.putString("returntype", "withvalue");// 返回值传递方式withvalue带参数的传值方式（新传值方式）
            intent.putExtras(bundle);
            activity.startActivityForResult(intent, IDCARD_RECO_RESULT);
        }else if (resultCode == activity.RESULT_OK && requestCode == IDCARD_RECO_RESULT) {
            // 读识别返回值,这里是文通的调用系统拍照然后送识别的回调接口.
            int ReturnAuthority = data.getIntExtra("ReturnAuthority", -100000);// 取激活状态
            System.out.println("ReturnAuthority:" + ReturnAuthority + "---"
                    + "nMainID:" + nMainID);
            int ReturnInitIDCard = data
                    .getIntExtra("ReturnInitIDCard", -100000);// 取初始化返回值
            int ReturnLoadImageToMemory = data.getIntExtra(
                    "ReturnLoadImageToMemory", -100000);// 取读图像的返回值
            int ReturnRecogIDCard = data.getIntExtra("ReturnRecogIDCard",
                    -100000);// 取识别的返回值
            LoggerUtils.i("识别核心版本号:" +
                    data.getStringExtra("ReturnGetVersionInfo"));

            LoggerUtils.i(
                    "ReturnLPFileName:"
                            + data.getStringExtra("ReturnLPFileName"));

            if (ReturnAuthority == 0 && ReturnInitIDCard == 0
                    && ReturnLoadImageToMemory == 0 && ReturnRecogIDCard > 0) {

                String result = "";
                String[] fieldname = (String[]) data
                        .getSerializableExtra("GetFieldName");
                String[] fieldvalue = (String[]) data
                        .getSerializableExtra("GetRecogResult");
                String time = data.getStringExtra("ReturnTime");
                String ReturnLPFileName = data.getStringExtra("ReturnLPFileName");
                LoggerUtils.i("ReturnLPFileName: ---> " + ReturnLPFileName);
                // 新建一个身份证信息对象
                People people = new People();
                if (null != fieldname) {
                    int count = fieldname.length;
                    for (int i = 0; i < count; i++) {
                        if (fieldname[i] == null) {
                            break;
                        }
                        /*result += fieldname[i] + ":" + fieldvalue[i]
                                + ";\n";
                        */
                        if (nMainID == 2) { // 身份证识别
                            switch (fieldname[i]) {
                                case "姓名":
                                    people.setPeopleName(fieldvalue[i]);
                                    break;
                                case "公民身份号码":
                                    people.setPeopleIDCode(fieldvalue[i]);
                                    break;
                                case "出生":
                                    people.setPeopleBirthday(fieldvalue[i]);
                                    break;
                                case "住址":
                                    people.setPeopleAddress(fieldvalue[i]);
                                    break;
                                case "性别":
                                    people.setPeopleSex(fieldvalue[i]);
                                    break;
                                case "民族":
                                    people.setPeopleNation(fieldvalue[i]);
                                    break;
                            }
                        } else if(nMainID == 5) { // 驾照识别结果封装
                            switch (fieldname[i]) {
                                case "姓名":
                                    people.setPeopleName(fieldvalue[i]);
                                    break;
                                case "证号":
                                    people.setPeopleIDCode(fieldvalue[i]);
                                    break;
                                case "出生日期":
                                    people.setPeopleBirthday(fieldvalue[i]);
                                    break;
                                case "住址":
                                    people.setPeopleAddress(fieldvalue[i]);
                                    break;
                                case "性别":
                                    people.setPeopleSex(fieldvalue[i]);
                                    break;
                                case "民族":
                                    people.setPeopleNation(fieldvalue[i]);
                                    break;
                            }
                        }
                    }

                }


                /*String str = "\n" + activity.getString(R.string.recogResult1) + " \n"
                        + activity.getString(R.string.idcardType) + ReturnRecogIDCard
                        + "\n" + result;*/
                // 把图片解析成byte数组
                if (new File(facePhoto +  "/face.jpg").exists()) {
                    byte[] bytes = FileUtils.getBytesFromFile(facePhoto + "/face.jpg");
                    people.setPhoto(bytes);
                }

                if (selectPath != null) {
                    people.setIDCardPhoto(selectPath);
                }
                // 这里是身份证信息,可以从这里回调
                if (recoLIDCARDistener != null)
                    recoLIDCARDistener.onSucessReco(people);
                LoggerUtils.i("识别的结果: ---> " + people.toString());
                sucessReco = false;
            } else {
                String str = "";
                if (ReturnAuthority == -100000) {
                    str = activity.getString(com.deao.io.wtlib.R.string.exception) + ReturnAuthority;
                } else if (ReturnAuthority != 0) {
                    str = activity.getString(com.deao.io.wtlib.R.string.exception1) + ReturnAuthority;
                } else if (ReturnInitIDCard != 0) {
                    str = activity.getString(com.deao.io.wtlib.R.string.exception2) + ReturnInitIDCard;
                } else if (ReturnLoadImageToMemory != 0) {
                    if (ReturnLoadImageToMemory == 3) {
                        str = activity.getString(com.deao.io.wtlib.R.string.exception3)
                                + ReturnLoadImageToMemory;
                    } else if (ReturnLoadImageToMemory == 1) {
                        str = activity.getString(com.deao.io.wtlib.R.string.exception4)
                                + ReturnLoadImageToMemory;
                    } else {
                        str = activity.getString(com.deao.io.wtlib.R.string.exception5)
                                + ReturnLoadImageToMemory;
                    }
                } else if (ReturnRecogIDCard != 0) {
                    str = activity.getString(com.deao.io.wtlib.R.string.exception6) + ReturnRecogIDCard;
                }
                // 识别失败
                sucessReco = false;
                LoggerUtils.i("识别的结果: ---> " + activity.getString(com.deao.io.wtlib.R.string.recogResult1) + " \n"
                        + str + "\n");
                recoLIDCARDistener.onFailedReco();
            }
        }
    }

    public void onActivityForResult(Fragment fragment,int requestCode, int resultCode, Intent data) {
        if (resultCode == fragment.getActivity().RESULT_OK && requestCode == RECO_SCAN_IDCARD_REQUESTCODE_RESULT) {
            // 身份证识别的返回结果
            nMainID = 2;
            if (data.getStringExtra(Constants.RESULT_RECO_ERROR_CODE) != null) { //
                // 有识别的错误代码说明识别失败
                recoLIDCARDistener.onFailedReco();
            } else {
                recoLIDCARDistener.onSucessReco((People) data.getParcelableExtra(Constants.RESULT_RECO_INTTENT_KEY));
            }

        }
        else if (resultCode == fragment.getActivity().RESULT_OK && requestCode == RECO_SCAN_DRIVER_REQUESTCODE_RESULT) {
            // 驾照识别的返回结果
            nMainID = 5;

            if (data.getStringExtra(Constants.RESULT_RECO_ERROR_CODE) != null) { //
                // 有识别的错误代码说明识别失败
                recoLIDCARDistener.onFailedReco();
            } else {
                recoDriverListener.onSucessRecoDriver((People) data.getParcelableExtra(Constants.RESULT_RECO_INTTENT_KEY));

            }
        }
    }

    public interface OnRecoIDCARDListener {
        // 身份证识别成功回调
        public void onSucessReco(People people);

        public void onFailedReco();
    }

    public interface onRecoDriverListener {
        // 驾照识别成功回调
        public void onSucessRecoDriver(People people);
    }

    /**
     * 获得指定文件的byte数组
     */
    public static byte[] getBytes(String filePath) {
        byte[] buffer = null;
        try {
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1000];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }

}
