package com.wintone.passport.sdk.uitls;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;


public class UriUtils {
    /**
     * 从URI路径中获得真实的路径地址.
     *
     * @param context
     * @param uri
     * @param projection
     * @param selection
     * @param selectionArgs
     * @param sortOrder
     * @return
     */
    public static String getFilePathFromUri(Context context, Uri uri,
                                            String[] projection, String selection, String[] selectionArgs,
                                            String sortOrder) {
        Cursor cursor = context.getContentResolver().query(uri, projection,
                selection, selectionArgs, sortOrder);
        int index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(index);
        cursor.close();
        cursor = null;
        return path;
    }

    public static String getUriRealPath(Context context, Uri uri) {
        if (uri == null)
            return "";
        return getFilePathFromUri(context, uri, null, null, null, null);
    }

}
