package com.wintone.passport.sdk.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.util.Arrays;

/**
 * Created by Administrator on 2016/6/13.
 */
public class People implements Parcelable {
    /**
     * 姓名
     */
    private String peopleName;

    /**
     * 性别,性别使用男/女
     */
    private String peopleSex;

    /**
     * 民族
     */
    private String peopleNation;

    /**
     * 出生日期
     */
    private String peopleBirthday;

    /**
     * 住址
     */
    private String peopleAddress;

    /**
     * 身份证号
     */
    private String peopleIDCode;

    /**
     * 签发机关
     */
    private String department;

    /**
     * 有效期限：开始
     */
    private String startDate;

    /**
     * 有效期限：结束
     */
    private String endDate;

    /**
     * 身份证头像
     */
    private byte[] photo;

    /**
     * 没有解析成图片的数据大小一般为1024字节
     */
    private byte[] headImage;

    /**
     * 三代证指纹模板数据，正常位1024，如果为null，说明为二代证，没有指纹模板数据
     */
    private byte[] model;
    /**
     * 拍摄的整个身份证的照片
     */
    private String IDCardPhoto;

    public People() {
    }

    protected People(Parcel in) {
        peopleName = in.readString();
        peopleSex = in.readString();
        peopleNation = in.readString();
        peopleBirthday = in.readString();
        peopleAddress = in.readString();
        peopleIDCode = in.readString();
        department = in.readString();
        startDate = in.readString();
        endDate = in.readString();
        photo = in.createByteArray();
        headImage = in.createByteArray();
        model = in.createByteArray();
        IDCardPhoto = in.readString();
    }

    public static final Creator<People> CREATOR = new Creator<People>() {
        @Override
        public People createFromParcel(Parcel in) {
            return new People(in);
        }

        @Override
        public People[] newArray(int size) {
            return new People[size];
        }
    };

    public String getIDCardPhotoPath() {
        return IDCardPhoto;
    }

    public void setIDCardPhoto(String IDCardPhoto) {
        this.IDCardPhoto = IDCardPhoto;
    }

    public String getPeopleName() {
        if (TextUtils.isEmpty(peopleName))
            return "";
        return peopleName;
    }

    public void setPeopleName(String peopleName) {
        this.peopleName = peopleName;
    }

    public String getPeopleSex() {
        return peopleSex;
    }

    public void setPeopleSex(String peopleSex) {
        this.peopleSex = peopleSex;
    }

    public String getPeopleNation() {
        return peopleNation;
    }

    public void setPeopleNation(String peopleNation) {
        this.peopleNation = peopleNation;
    }

    public String getPeopleBirthday() {
        return peopleBirthday;
    }

    public void setPeopleBirthday(String peopleBirthday) {
        this.peopleBirthday = peopleBirthday;
    }

    public String getPeopleAddress() {
        if(TextUtils.isEmpty(peopleAddress))
            return "";
        return peopleAddress;
    }

    public void setPeopleAddress(String peopleAddress) {
        this.peopleAddress = peopleAddress;
    }

    public String getPeopleIDCode() {
        return peopleIDCode;
    }

    public void setPeopleIDCode(String peopleIDCode) {
        this.peopleIDCode = peopleIDCode;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public byte[] getHeadImage() {
        return headImage;
    }

    public void setHeadImage(byte[] headImage) {
        this.headImage = headImage;
    }

    public byte[] getModel() {
        return model;
    }

    public void setModel(byte[] model) {
        this.model = model;
    }


    @Override
    public String toString() {
        return "People{" +
                "peopleName='" + peopleName + '\'' +
                ", peopleSex='" + peopleSex + '\'' +
                ", peopleNation='" + peopleNation + '\'' +
                ", peopleBirthday='" + peopleBirthday + '\'' +
                ", peopleAddress='" + peopleAddress + '\'' +
                ", peopleIDCode='" + peopleIDCode + '\'' +
                ", department='" + department + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", photo=" + Arrays.toString(photo) +
                ", headImage=" + Arrays.toString(headImage) +
                ", model=" + Arrays.toString(model) +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(peopleName);
        dest.writeString(peopleSex);
        dest.writeString(peopleNation);
        dest.writeString(peopleBirthday);
        dest.writeString(peopleAddress);
        dest.writeString(peopleIDCode);
        dest.writeString(department);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeByteArray(photo);
        dest.writeByteArray(headImage);
        dest.writeByteArray(model);
        dest.writeString(IDCardPhoto);
    }
}
