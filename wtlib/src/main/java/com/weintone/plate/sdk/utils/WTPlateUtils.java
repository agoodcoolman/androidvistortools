package com.weintone.plate.sdk.utils;

import android.app.Activity;
import android.app.Fragment;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;

import com.deao.io.wtlib.R;
import com.deao.io.wtlib.old.LoggerUtils;
import com.weintone.plate.sdk.CameraActivity;
import com.wintone.passport.sdk.model.Constants;
import com.wintone.passport.sdk.uitls.Devcode;
import com.wintone.plateid.PlateCfgParameter;
import com.wintone.plateid.PlateRecognitionParameter;
import com.wintone.plateid.RecogService;

import java.io.File;
import java.util.List;

import wintone.idcard.android.AuthService;

/**
 * Created by Administrator on 2016/6/12.
 * 文通车牌识别工具类
 *
 * 使用方法:
 * 1.在要调用的类中newInstance获取到当前类的实例对象
 * 2.在调用类中使用使用startRecoPlate 方法进行识别
 * 3.必须调用类的onActivityResult 方法中调用当前的WTPlateUtils类中的onActivityResult方法,否则车辆照片无法正确的回调
 * 4.在调用的类中实现当前类中的OnRecoPlateListener 接口,以获取识别后的
 *
 * 修改:2016/6/12
 * 1.添加了在Fragment中的使用
 *
 * 2016/8/17
 * 1.当前类的Context设置为了ApplicationContext 为了避免内存泄露
 *
 */
public class WTPlateUtils {
    private final int RECO_PLATE_PHOTO = 0x001009;
    private Context context;
    private static WTPlateUtils wtPlateUtils;
    //识别车牌的线程中识别的方法
    public RecogService.MyBinder recogBinder;
    private AuthService.authBinder authBinder;
    private int imageformat = 1;
    private int iInitPlateIDSDK = -1;
    private int bVertFlip = 0;
    private int bDwordAligned = 1;
    private String[] fieldvalue = new String[14];
    int[] fieldname = {R.string.plate_number, R.string.plate_color,
            R.string.plate_color_code, R.string.plate_type_code,
            R.string.plate_reliability, R.string.plate_brightness_reviews,
            R.string.plate_move_orientation, R.string.plate_leftupper_pointX,
            R.string.plate_leftupper_pointY, R.string.plate_rightdown_pointX,
            R.string.plate_rightdown_pointY, R.string.plate_elapsed_time,
            R.string.plate_light, R.string.plate_car_color};
    private int nRet = -1;
    private int width = 420;
    private int height = 232;
    private static OnRecoPlateListener listener;
    public static final String PATH = Environment.getExternalStorageDirectory().toString() + "/AndroidWT";
    private String recogPicPath ;
    // 车牌识别
    protected ServiceConnection recogConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            recogConn = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            recogBinder = (RecogService.MyBinder) service;

            iInitPlateIDSDK = recogBinder.getInitPlateIDSDK();
            if (iInitPlateIDSDK != 0) {

                nRet = iInitPlateIDSDK;
                LoggerUtils.i("测试" + nRet);

                String[] str = {"" + iInitPlateIDSDK};
                getResult(str);
            } else {
                PlateCfgParameter cfgparameter = new PlateCfgParameter();
                cfgparameter.armpolice = 4;// 单层武警车牌是否开启:4是；5不是
                cfgparameter.armpolice2 = 16;// 双层武警车牌是否开启:16是；17不是
                cfgparameter.embassy = 12;// 使馆车牌是否开启:12是；13不是
                cfgparameter.individual = 0;// 是否开启个性化车牌:0是；1不是
                //cfgparameter.nContrast = 9;// 清晰度指数(取值范围0-9,最模糊时设为1;最清晰时设为9)
                cfgparameter.nOCR_Th = 0;
                cfgparameter.nPlateLocate_Th = 5;// 识别阈值(取值范围0-9,5:默认阈值0:最宽松的阈值9:最严格的阈值)
                cfgparameter.onlylocation = 15;// 只定位车牌是否开启:14是；15不是
                cfgparameter.tworowyellow = 2;// 双层黄色车牌是否开启:2是；3不是
                cfgparameter.tworowarmy = 6;// 双层军队车牌是否开启:6是；7不是
                cfgparameter.szProvince = "";// 省份顺序
                cfgparameter.onlytworowyellow = 11;// 只识别双层黄牌是否开启:10是；11不是
                cfgparameter.tractor = 8;// 农用车车牌是否开启:8是；9不是
                cfgparameter.bIsNight = 1;// 是否夜间模式：1是；0不是  //废弃参数
                recogBinder.setRecogArgu(cfgparameter, imageformat, bVertFlip, bDwordAligned);
                PlateRecognitionParameter prp = new PlateRecognitionParameter();
                prp.height = height;// 图像高度
                prp.width = width;// 图像宽度
                prp.pic = recogPicPath;// 图像文件
                prp.isCheckDevType = false;//检查设备型号授权所用参数,另一个参数为devCode
                prp.devCode = Devcode.devcode;
                fieldvalue = recogBinder.doRecogDetail(prp);
                nRet = recogBinder.getnRet();

                if (nRet != 0) {
                    String[] str = {"" + nRet};
                    getResult(str);
                } else {
                    getResult(fieldvalue);
                }

            }

            if (recogBinder != null) {
                context.unbindService(recogConn);
            }

        }
    };
    private boolean hadWirtePreferences;

    // 构造函数
    private WTPlateUtils(Context context) {
        this.context = context;
        // 初始化拍照的相关
        getCameraInformation();
    }

    public static WTPlateUtils newinitlization(Context context) {
        if (wtPlateUtils == null) {
            synchronized (WTPlateUtils.class) {
                if (wtPlateUtils == null)
                    wtPlateUtils = new WTPlateUtils(context.getApplicationContext());
            }
        }
        return wtPlateUtils;
    }

    // 开启身份证识别
    public void startRecoPlate(Activity context) {

        if (context instanceof OnRecoPlateListener)
            listener = (OnRecoPlateListener)context;
        // 启动文通识别车牌的类
        if (readIntPreferences("PlateService", "picWidth") != 0 && readIntPreferences("PlateService", "picHeight") != 0
                && readIntPreferences("PlateService", "preWidth") != 0 && readIntPreferences("PlateService", "preHeight") != 0
                && readIntPreferences("PlateService", "preMaxWidth") != 0 && readIntPreferences("PlateService", "preMaxHeight") != 0 || hadWirtePreferences) {
            Intent camera_intent = new Intent(context, CameraActivity.class);
            context.startActivityForResult(camera_intent, RECO_PLATE_PHOTO);

        } else {
            Intent sysCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            String sdDirString = PATH + "/wintoneimage";
            long datetime = System.currentTimeMillis();
            recogPicPath = sdDirString + "/plateid" + datetime + ".jpg";
            sysCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(recogPicPath)));

            sysCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            context.startActivityForResult(sysCameraIntent, RECO_PLATE_PHOTO);
        }

    }

    // 开启身份证识别
    public void startRecoPlate(Fragment context) {

        if (context instanceof OnRecoPlateListener)
            listener = (OnRecoPlateListener)context;
        // 启动文通识别车牌的类
        if (readIntPreferences("PlateService", "picWidth") != 0 && readIntPreferences("PlateService", "picHeight") != 0
                && readIntPreferences("PlateService", "preWidth") != 0 && readIntPreferences("PlateService", "preHeight") != 0
                && readIntPreferences("PlateService", "preMaxWidth") != 0 && readIntPreferences("PlateService", "preMaxHeight") != 0) {
            Intent camera_intent = new Intent(context.getActivity(), CameraActivity.class);
            context.startActivityForResult(camera_intent, RECO_PLATE_PHOTO);

        } else {
            Intent sysCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            String sdDirString = PATH + "/wintoneimage";
            long datetime = System.currentTimeMillis();
            recogPicPath = sdDirString + "/plateid" + datetime + ".jpg";
            sysCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(recogPicPath)));

            sysCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            context.startActivityForResult(sysCameraIntent, RECO_PLATE_PHOTO);
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == RECO_PLATE_PHOTO && resultCode == Activity.RESULT_OK) {
            String platePhoto = data.getStringExtra(Constants.PLATE_PAOTO);
            if (platePhoto == null) { // 返回的数据为空,说明是从拍照返回的照片.拍照的地址就是 :recogPicPath

            } else {
                recogPicPath = platePhoto;
            }
            Intent recogIntent = new Intent(context, RecogService.class);
            context.bindService(recogIntent, recogConn, Service.BIND_AUTO_CREATE);

        } else {
            // 没有拍照,没有执行其它操作.
            release();
        }
    }
    //获得结果
    private void getResult(String[] fieldvalue) {

        Plate plate = new Plate();
        plate.setPlatePhotoPath(recogPicPath);
        if (nRet != 0) {
            String nretString = nRet + "";
            if (nretString.equals("-1001")) {
                plate.setError("-1001");
                plate.setErrorContent(getString(R.string.failed_readJPG_error));

            } else if (nretString.equals("-10001")) {
                plate.setError("-10001");
                plate.setErrorContent(getString(R.string.failed_noInit_function));

            } else if (nretString.equals("-10003")) {
                plate.setError("-10003");
                plate.setErrorContent(getString(R.string.failed_validation_faile));


            } else if (nretString.equals("-10004")) {
                plate.setError("-10004");
                plate.setErrorContent(getString(R.string.failed_serial_number_null));

            } else if (nretString.equals("-10005")) {
                plate.setError("-10005");
                plate.setErrorContent(getString(R.string.failed_disconnected_server));

            } else if (nretString.equals("-10006")) {
                plate.setError("-10006");
                plate.setErrorContent(getString(R.string.failed_obtain_activation_code));

            } else if (nretString.equals("-10007")) {
                plate.setError("-10007");
                plate.setErrorContent(getString(R.string.failed_noexist_serial_number));

            } else if (nretString.equals("-10008")) {
                plate.setError("-10008");
                plate.setErrorContent(getString(R.string.failed_serial_number_used));

            } else if (nretString.equals("-10009")) {
                plate.setError("-10009");
                plate.setErrorContent(getString(R.string.failed_unable_create_authfile));

            } else if (nretString.equals("-10010")) {
                plate.setError("-10010");
                plate.setErrorContent(getString(R.string.failed_check_activation_code));

            } else if (nretString.equals("-10011")) {
                plate.setError("-10011");
                plate.setErrorContent(getString(R.string.failed_other_errors));

            } else if (nretString.equals("-10012")) {
                plate.setError("-10012");
                plate.setErrorContent(getString(R.string.failed_not_active));

            } else if (nretString.equals("-10015")) {
                plate.setError("-10015");
                plate.setErrorContent(getString(R.string.failed_check_failure));

            } else {
                plate.setError("-10000");
                plate.setErrorContent("unknow");

            }
            if (listener!= null)
                listener.onFailed(plate);
        } else {
            String result = "";
            String[] resultString;
            String timeString = "";
            String boolString = "";
            boolString = fieldvalue[0];
            if (boolString != null && !boolString.equals("")) {
                resultString = boolString.split(";");
                int lenght = resultString.length;
                if (lenght == 1) {
                    // result = "";
                    if (fieldvalue[11] != null && !fieldvalue[11].equals("")) {
                        int time = Integer.parseInt(fieldvalue[11]);
                        time = time / 1000;
                        timeString = "" + time;
                    } else {
                        timeString = "null";
                    }

                    if (null != fieldname) {
                        result += getString(fieldname[0]) + ":" + fieldvalue[0]
                                + ";\n";
                        result += getString(fieldname[1]) + ":" + fieldvalue[1]
                                + ";\n";
                        result += getString(R.string.recognize_time) + ":"
                                + timeString + "ms" + ";\n";
                    }
                } else {
                    String itemString = "";
                    // result = "";
                    for (int i = 0; i < lenght; i++) {
                        itemString = fieldvalue[0];
                        resultString = itemString.split(";");
                        result += getString(fieldname[0]) + ":"
                                + resultString[i] + ";\n";

                        itemString = fieldvalue[1];
                        resultString = itemString.split(";");
                        result += getString(fieldname[1]) + ":"
                                + resultString[i] + ";\n";
                        itemString = fieldvalue[11];
                        resultString = itemString.split(";");
                        if (resultString[i] != null
                                && !resultString[i].equals("")) {
                            int time = Integer.parseInt(resultString[i]);
                            time = time / 1000;
                            timeString = "" + time;
                        } else {
                            timeString = "null";
                        }
                        result += getString(R.string.recognize_time) + ":"
                                + timeString + "ms" + ";\n";
                        result += "\n";
                        result += "\n";
                    }

                }
            } else {
                result += getString(fieldname[0]) + ":" + fieldvalue[0] + ";\n";
                result += getString(fieldname[1]) + ":" + fieldvalue[1] + ";\n";
                result += getString(R.string.recognize_time) + ":" + "null"
                        + ";\n";
            }

            // 结果进行切割
            String[] split = result.split("\n");
            String color = "";
            String carNum = "";
            for (int i = 0; i < split.length; i++) {
                // 切割,当有数据的时候
                if (split[i].startsWith("车牌号")
                        && split[i].split(":").length > 1) {
                    // 这里车牌号码信息通过hanler进行操作.
                    carNum = split[i].split(":")[1];
                }
                if (split[i].startsWith("车牌颜色")
                        && split[i].split(":").length > 1) {
                    // 这里车牌号码信息通过hanler进行操作.
                    color = split[i].split(":")[1];
                }
            }
            color = color.substring(0, color.length() - 1);
            carNum = carNum.substring(0, carNum.length() - 1);

            plate.setPlateColor(color);
            plate.setPlateNumber(carNum);

            LoggerUtils.i(plate);
            if (listener != null)
                listener.onSucess(plate);
        }
        nRet = -1;
        fieldvalue = null;

        release();
    }

    public void release() {
        listener = null;
        recogPicPath = null;
        wtPlateUtils = null;

    }

    public interface OnRecoPlateListener {
        public void onSucess(Plate plate);

        public void onFailed(Plate plate);
    }

    // 汽车识别相关信息的对象
    public class Plate{
        // 错误代码
        private String error;
        // 错误的内容
        private String errorContent;

        private String plateNumber;

        private String plateColor;

        private String platePhotoPath;

        public String getPlatePhotoPath() {
            return platePhotoPath;
        }

        public void setPlatePhotoPath(String platePhotoPath) {
            this.platePhotoPath = platePhotoPath;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }

        public String getErrorContent() {
            return errorContent;
        }

        public void setErrorContent(String errorContent) {
            this.errorContent = errorContent;
        }

        public String getPlateNumber() {
            return plateNumber;
        }

        public void setPlateNumber(String plateNumber) {
            this.plateNumber = plateNumber;
        }

        public String getPlateColor() {
            return plateColor;
        }

        public void setPlateColor(String plateColor) {
            this.plateColor = plateColor;
        }

        @Override
        public String toString() {
            return "Plate{" +
                    "error='" + error + '\'' +
                    ", errorContent='" + errorContent + '\'' +
                    ", plateNumber='" + plateNumber + '\'' +
                    ", plateColor='" + plateColor + '\'' +
                    ", platePhotoPath='" + platePhotoPath + '\'' +
                    '}';
        }
    }

    public final String getString(int resId) {
        return context.getResources().getString(resId);
    }

    //=====文通车牌识别======开始======================================================================
    //获得照相机得相关参数.
    private void getCameraInformation() {
        if (readIntPreferences("PlateService", "picWidth") == 0
                || readIntPreferences("PlateService", "picHeight") == 0
                || readIntPreferences("PlateService", "preWidth") == 0
                || readIntPreferences("PlateService", "preHeight") == 0
                || readIntPreferences("PlateService", "preMaxWidth") == 0
                || readIntPreferences("PlateService", "preMaxHeight") == 0) {

            Camera camera = null;
            int pre_Max_Width = 640;
            int pre_Max_Height = 480;
            final int Max_Width = 2048;
            final int Max_Height = 1536;
            boolean isCatchPicture = false;
            int picWidth = 2048;
            int picHeight = 1536;
            int preWidth = 320;
            int preHeight = 240;
            try {
                camera = Camera.open();
                if (camera != null) {
                    Camera.Parameters parameters = camera.getParameters();
                    List<Camera.Size> previewSizes = parameters
                            .getSupportedPreviewSizes();
                    Camera.Size size;
                    int second_Pre_Width = 0, second_Pre_Height = 0;
                    int length = previewSizes.size();
                    if (length == 1) {
                        size = previewSizes.get(0);
                        pre_Max_Width = size.width;
                        pre_Max_Height = size.height;
                    } else {
                        for (int i = 0; i < length; i++) {
                            size = previewSizes.get(i);
                            if (size.width <= Max_Width
                                    && size.height <= Max_Height) {
                                second_Pre_Width = size.width;
                                second_Pre_Height = size.height;
                                if (pre_Max_Width < second_Pre_Width) {
                                    pre_Max_Width = second_Pre_Width;
                                    pre_Max_Height = second_Pre_Height;
                                }
                            }
                        }
                    }

                    for (int i = 0; i < previewSizes.size(); i++) {
                        if (previewSizes.get(i).width == 640
                                && previewSizes.get(i).height == 480) {
                            preWidth = 640;
                            preHeight = 480;
                            break;
                        }
                        if (previewSizes.get(i).width == 320
                                && previewSizes.get(i).height == 240) {
                            preWidth = 320;
                            preHeight = 240;
                        }
                    }
                    if (preWidth == 0 || preHeight == 0) {
                        if (previewSizes.size() == 1) {
                            preWidth = previewSizes.get(0).width;
                            preHeight = previewSizes.get(0).height;
                        } else {
                            preWidth = previewSizes
                                    .get(previewSizes.size() / 2).width;
                            preHeight = previewSizes
                                    .get(previewSizes.size() / 2).height;
                        }
                    }

                    List<Camera.Size> PictureSizes = parameters
                            .getSupportedPictureSizes();
                    for (int i = 0; i < PictureSizes.size(); i++) {
                        if (PictureSizes.get(i).width == 2048
                                && PictureSizes.get(i).height == 1536) {
                            if (isCatchPicture == true) {
                                break;
                            }
                            isCatchPicture = true;
                            picWidth = 2048;
                            picHeight = 1536;
                        }
                        if (PictureSizes.get(i).width == 1600
                                && PictureSizes.get(i).height == 1200) {
                            isCatchPicture = true;
                            picWidth = 1600;
                            picHeight = 1200;
                        }
                        if (PictureSizes.get(i).width == 1280
                                && PictureSizes.get(i).height == 960) {
                            isCatchPicture = true;
                            picWidth = 1280;
                            picHeight = 960;
                            break;
                        }
                    }
                }

                writeIntPreferences("PlateService", "picWidth", picWidth);
                writeIntPreferences("PlateService", "picHeight", picHeight);
                writeIntPreferences("PlateService", "preWidth", preWidth);
                writeIntPreferences("PlateService", "preHeight", preHeight);
                writeIntPreferences("PlateService", "preMaxWidth",
                        pre_Max_Width);
                writeIntPreferences("PlateService", "preMaxHeight",
                        pre_Max_Height);
                // 这里加一个判断,是因为第一次进来,然后这里运行了保存操作,但是还没有保存完毕,就已经开始运行执行识别操作(获取preferences里面的值,然后获取的是空的,就启动了系统的拍照模式.加一个boolean去判断)
                hadWirtePreferences = true;
            } catch (Exception e) {

            } finally {
                if (camera != null) {
                    try {
                        camera.release();
                        camera = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    // 读写sp对象.
    protected int readIntPreferences(String perferencesName, String key) {
        SharedPreferences preferences = context.getSharedPreferences(perferencesName,
                Context.MODE_PRIVATE);
        int result = preferences.getInt(key, 0);
        return result;
    }

    protected void writeIntPreferences(String perferencesName, String key,
                                       int value) {
        SharedPreferences preferences = context.getSharedPreferences(perferencesName,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

}
