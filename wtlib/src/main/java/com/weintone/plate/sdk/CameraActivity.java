package com.weintone.plate.sdk;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.deao.io.wtlib.R;
import com.wintone.passport.sdk.model.Constants;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * 车牌的拍照的相机类.
 *
 * 2016/8/17
 * 1.在Ondestory中添加释放类,避免内存泄露
 */

public class CameraActivity extends Activity implements SurfaceHolder.Callback{
    public static final String PATH = Environment.getExternalStorageDirectory().toString()+"/wintoneimage/";
    private int preWidth = 320;
    private int preHeight = 240;
    private int picWidth = 2048;
    private int picHeight = 1536;
    private Camera camera;
    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private ToneGenerator tone;
    private ImageView imageView;
    private Bitmap bitmap;
    private RelativeLayout rlyaout;
    private String recogPicPath;
    private int width,height;
    private boolean hasFlashLigth = false;
    private long fastClick = 0; 
    private ImageButton backImage,restartImage,takeImage,recogImage,lightOn,lightOff;
    private TextView backAndRestartText,takeAndRecogText,lightText;
    private ImageView leftImage,rightImage;
    private boolean taking = false;
    private boolean recoging = false;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PlateProjectTool.cleanActivityList();
    }

    protected void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		DisplayMetrics displayMetrics = new DisplayMetrics();  
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics); 
        width = Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels);
        height = Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels);
        setContentView(R.layout.wintone_camera);
        PlateProjectTool.addActivityList(CameraActivity.this);
        picWidth = readIntPreferences("PlateService","picWidth");
        picHeight = readIntPreferences("PlateService","picHeight");
        preWidth = readIntPreferences("PlateService","preWidth");
        preHeight =	readIntPreferences("PlateService","preHeight");
        findViewAndLayout();
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(CameraActivity.this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        FeatureInfo[] features = this.getPackageManager().getSystemAvailableFeatures();
        for (FeatureInfo featureInfo : features) {

            if(PackageManager.FEATURE_CAMERA_FLASH.equals(featureInfo.name)) {
                hasFlashLigth = true;
            }
        }
    }
    
	private void findViewAndLayout() {
		int layoutWidth = (int)(width-((height*4)/3));
		RelativeLayout.LayoutParams layoutParams= new RelativeLayout.LayoutParams(layoutWidth, height);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        rlyaout = (RelativeLayout) findViewById(R.id.rlyaout);
        rlyaout.setLayoutParams(layoutParams);
        
        imageView = (ImageView) findViewById(R.id.BimageView);
        surfaceView = (SurfaceView) findViewById(R.id.surfaceViwe);
        
        int layout_hieght = (int) (height*0.12);
    	layoutParams  = new RelativeLayout.LayoutParams(layout_hieght, layout_hieght);
    	layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
    	layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
    	layoutParams.topMargin = layout_hieght;
    	backImage = (ImageButton) findViewById(R.id.backimage);
    	backImage.setBackgroundResource(R.drawable.back);
    	backImage.setVisibility(View.VISIBLE);
    	backImage.setLayoutParams(layoutParams);
    	restartImage = (ImageButton) findViewById(R.id.restartimage);
    	restartImage.setBackgroundResource(R.drawable.rephotograph);
    	restartImage.setLayoutParams(layoutParams);
    	restartImage.setVisibility(View.INVISIBLE);
    	layoutParams  = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
    	layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
    	layoutParams.addRule(RelativeLayout.BELOW, R.id.restartimage);
    	backAndRestartText = (TextView) findViewById(R.id.backandrestarttext);
    	backAndRestartText.setVisibility(View.VISIBLE);
    	backAndRestartText.setText("返回");
    	backAndRestartText.setLayoutParams(layoutParams);
    	backAndRestartText.setTextColor(Color.BLACK);
    	layoutParams  = new RelativeLayout.LayoutParams(layout_hieght, layout_hieght);
    	layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
    	takeImage = (ImageButton) findViewById(R.id.takeimage);
    	takeImage.setBackgroundResource(R.drawable.takepic);
    	takeImage.setLayoutParams(layoutParams);
    	takeImage.setVisibility(View.VISIBLE);
    	recogImage = (ImageButton) findViewById(R.id.recogimage);
    	recogImage.setBackgroundResource(R.drawable.recognition);
    	recogImage.setLayoutParams(layoutParams);
    	recogImage.setVisibility(View.INVISIBLE);
    	layoutParams  = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
    	layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
    	layoutParams.addRule(RelativeLayout.BELOW, R.id.recogimage);
    	takeAndRecogText = (TextView) findViewById(R.id.takeandrecogtext);
    	takeAndRecogText.setText("拍照");
    	takeAndRecogText.setLayoutParams(layoutParams);
    	takeAndRecogText.setVisibility(View.VISIBLE);
    	takeAndRecogText.setTextColor(Color.BLACK);
    	lightOn = (ImageButton) findViewById(R.id.lightimage1);
    	lightOn.setBackgroundResource(R.drawable.light1);
    	lightOn.setVisibility(View.VISIBLE);
    	lightOff = (ImageButton) findViewById(R.id.lightimage2);
    	lightOff.setBackgroundResource(R.drawable.light2);
    	lightOff.setVisibility(View.INVISIBLE);
    	layoutParams  = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
    	layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
    	layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
    	layoutParams.bottomMargin = layout_hieght;
        lightText = (TextView) findViewById(R.id.lighttext);
        lightText.setText("闪光");
        lightText.setLayoutParams(layoutParams);
        lightText.setVisibility(View.VISIBLE);
        lightText.setTextColor(Color.BLACK);
    	layoutParams  = new RelativeLayout.LayoutParams(layout_hieght, layout_hieght);
    	layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
    	layoutParams.addRule(RelativeLayout.ABOVE, R.id.lighttext);
    	lightOn.setLayoutParams(layoutParams);
    	lightOff.setLayoutParams(layoutParams);
        
    	leftImage = (ImageView) findViewById(R.id.cameraframeleft);
    	leftImage.setBackgroundResource(R.drawable.frame_left);
        rightImage = (ImageView) findViewById(R.id.cameraframeright);
        rightImage.setBackgroundResource(R.drawable.frame_right);
        
        int surface_width = (int)((height*4)/3);
        int pic_width = readIntPreferences("PlateService","picWidth");
        int frame_width = (160*surface_width)/pic_width;
        if(frame_width <= (int)(height*0.16)){
        	frame_width = (int) (height*0.16 + 80);
        }else{
        	frame_width = frame_width+40;
        }
    	int margin = (surface_width-frame_width)/2;

    	RelativeLayout.LayoutParams frame_left = new RelativeLayout.LayoutParams((int)(height*0.08), (int)(height*0.125));
    	frame_left.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
    	frame_left.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
    	frame_left.leftMargin = margin;
    	leftImage.setLayoutParams(frame_left);
    	
    	RelativeLayout.LayoutParams frame_right = new RelativeLayout.LayoutParams((int)(height*0.08), (int)(height*0.125));
    	frame_right.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
    	frame_right.addRule(RelativeLayout.LEFT_OF, R.id.rlyaout);
    	frame_right.rightMargin = margin;
    	rightImage.setLayoutParams(frame_right);
    	
        showFrameImageView();
        
    	backImage.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

	        	freeCamera();
	        	/*Intent intent = new Intent(getApplicationContext(),MainActivity.class);
	        	startActivity(intent);*/
	        	CameraActivity.super.finish();
			}
    	});

    	restartImage.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				showFrameImageView();
	            takeImage.setVisibility(View.VISIBLE);
	            recogImage.setVisibility(View.INVISIBLE);
	            restartImage.setVisibility(View.INVISIBLE);
	            backImage.setVisibility(View.VISIBLE);
				backAndRestartText.setText("重拍");
				takeAndRecogText.setText("识别");
		        if(bitmap != null) {
					if (!bitmap.isRecycled()) {
			        	bitmap.recycle();
					}
		        	bitmap = null;
		        }
		        imageView.setVisibility(View.INVISIBLE);
		        imageView.setImageDrawable(null);
	            camera.startPreview();
			}
		});
        
    	takeImage.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(!taking){
					taking = true;
					takePicture();
				}else {
					System.out.println("!!!");
				}
			}
		});
    	
    	recogImage.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(recoging){
					System.out.println("");
				}else{
					saveAndRecogPic();
				}
			}
		});
        
    	lightOn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				lightText.setText("打开");
	            lightOn.setVisibility(View.INVISIBLE);
	            lightOff.setVisibility(View.VISIBLE);
	            if(hasFlashLigth) {
	                openFlahsLight();
	            }

			}
		});
        
    	lightOff.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				lightText.setText("关闭");
	            lightOn.setVisibility(View.VISIBLE);
	            lightOff.setVisibility(View.INVISIBLE);
	            if(hasFlashLigth) {
	                closeFlashLigth();
	            }
			}
		});

    }
    
	public void  freeCamera() {
    	try {
            if (camera != null) {
                camera.cancelAutoFocus();
                camera.setPreviewCallback(null);
                camera.stopPreview();
                camera.release();
                camera = null;
            }
		} catch (Exception camera_exception) {
			if (PlateProjectTool.mCamera != null) {
				PlateProjectTool.mCamera.release();
				PlateProjectTool.mCamera = null;
			}
		}

        if(bitmap != null) {
        	if (!bitmap.isRecycled()) {
            	bitmap.recycle();
			}
        	bitmap = null;
        }
        imageView.setImageDrawable(null);
	}
	

    protected int readIntPreferences(String perferencesName, String key) {
    	SharedPreferences preferences = getSharedPreferences(perferencesName, MODE_PRIVATE);
	    int result = preferences.getInt(key, 0);
	    return result;
    }
	
    private void showFrameImageView(){
    	leftImage.setVisibility(View.VISIBLE);
    	rightImage.setVisibility(View.VISIBLE);
    }
	
    private void hideFrameImageView(){
    	leftImage.setVisibility(View.INVISIBLE);
    	rightImage.setVisibility(View.INVISIBLE);
    }    
    
    public boolean isEffectClick() {
        long lastClick = System.currentTimeMillis();
        long diffTime = lastClick - fastClick;
        if(diffTime > 5000) {
            fastClick = lastClick;
            return true;
        }
        return false;
    }
    
    
    public void openFlahsLight() {
    	if(camera != null){
            Camera.Parameters parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(parameters);
            camera.autoFocus(new AutoFocusCallback() {
                public void onAutoFocus(boolean success, Camera camera) {
                }
            });
            camera.startPreview();
    	}

    }
    
    public void closeFlashLigth() {
        if (camera != null) {
            camera.stopPreview();
            Camera.Parameters parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(parameters);
            camera.startPreview();

        }
    }

    //
    private void saveAndRecogPic(){
    	recoging = true;
        try {
            File dir = new File(PATH);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            long datetime = System.currentTimeMillis();
            recogPicPath =  PATH +"plateid"+datetime+".jpg";
            File file = new File(recogPicPath);
            if(file.exists()){
            	file.delete();
            }
        	file.createNewFile();
            BufferedOutputStream bos = new BufferedOutputStream(
                    new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
        } catch (Exception e) {
        	recogPicPath = "";
        }

        Intent intent = new Intent();
        intent.putExtra(Constants.PLATE_PAOTO, recogPicPath);
        setResult(RESULT_OK, intent);
        super.finish();


    }

    public void takePicture() {
        if (camera != null) {
            try {
                camera.autoFocus(new AutoFocusCallback() {
                    public void onAutoFocus(boolean success, Camera camera) {
                    	camera.takePicture(shutterCallback, null, PictureCallback);
                    }
                });
            } catch (Exception e) {
            	e.printStackTrace();
                Toast.makeText(this, R.string.toast_autofocus_failure, Toast.LENGTH_SHORT).show();
            }
        }
    }
    
    private PictureCallback PictureCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
        	camera.stopPreview();
            if(bitmap != null) {
            	if (!bitmap.isRecycled()) {
                	bitmap.recycle();
    			}
            	bitmap = null;
            }
            BitmapFactory.Options opts = new BitmapFactory.Options(); 
            opts.inInputShareable = true;
            opts.inPurgeable = true;
            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, opts);
            imageView.setImageBitmap(bitmap);
            takeImage.setVisibility(View.INVISIBLE);
            recogImage.setVisibility(View.VISIBLE);
            restartImage.setVisibility(View.VISIBLE);
            backImage.setVisibility(View.INVISIBLE);
			takeAndRecogText.setText("拍照识别");
			backAndRestartText.setText("返回重拍");
			hideFrameImageView();
			taking = false;
        }
    };
    
    private ShutterCallback shutterCallback = new ShutterCallback() {
        public void onShutter() {
        	try {
                if (tone == null){
                	tone = new ToneGenerator(1,ToneGenerator.MIN_VOLUME);
                }
                tone.startTone(ToneGenerator.TONE_PROP_BEEP);
			} catch (Exception e) {
				e.printStackTrace();
			}

        }
    };
    
    public void finish() {
    	try {
            if (camera != null) {
                camera.release();
                camera = null;
            }
		} catch (Exception camera_exception) {
			if (PlateProjectTool.mCamera != null) {
				PlateProjectTool.mCamera.release();
				PlateProjectTool.mCamera = null;
			}
		}

        if(bitmap != null) {
        	if (!bitmap.isRecycled()) {
            	bitmap.recycle();
			}
        	bitmap = null;
        }
    };
    
    public void surfaceCreated(SurfaceHolder holder) {
		if (camera == null) {	
    		try {
    			camera = Camera.open();
    			PlateProjectTool.mCamera = camera;
			} catch (Exception e) {
				if (PlateProjectTool.mCamera != null) {
					PlateProjectTool.mCamera.release();
					camera = Camera.open();
					PlateProjectTool.mCamera = camera;
				}
			}
		}
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		if (camera != null) {
			try {
				Camera.Parameters parameters = camera.getParameters();
				parameters.setPictureFormat(PixelFormat.JPEG);
				parameters.setPreviewSize(preWidth, preHeight);
				parameters.setPictureSize(picWidth, picHeight);
				camera.setParameters(parameters);
				camera.setPreviewDisplay(holder);
				camera.startPreview();
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getApplicationContext(),"���û�п���", Toast.LENGTH_LONG).show();
			}
		}

	}


    public void surfaceDestroyed(SurfaceHolder holder) {
    	try {
            if (camera != null) {
                camera.cancelAutoFocus();
                camera.setPreviewCallback(null);
                camera.stopPreview();
                camera.release();
                camera = null;
            }
            
		} catch (Exception camera_exception) {
			if (PlateProjectTool.mCamera != null) {
				PlateProjectTool.mCamera.release();
				PlateProjectTool.mCamera = null;
			}
			
		} 
    }
    //
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0){
        	freeCamera();
//        	Intent intent = new Intent(getApplicationContext(),MainActivity.class);
//        	startActivity(intent);
        	super.finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
