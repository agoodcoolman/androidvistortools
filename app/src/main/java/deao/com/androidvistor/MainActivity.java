package deao.com.androidvistor;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.deao.io.wtlib.old.utils.WTIDCARDTakePhotoUtils;
import com.wintone.passport.sdk.model.People;

import deao.com.androidvistortools.printer.PrinterUtils;

public class MainActivity extends AppCompatActivity implements WTIDCARDTakePhotoUtils.OnRecoIDCardListener {

    private PrinterUtils printerUtils;
    private WTIDCARDTakePhotoUtils newinitlization;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        newinitlization = WTIDCARDTakePhotoUtils.newinitlization();

    }


    public void button(View view) {
        printerUtils = new PrinterUtils(this);
        printerUtils.onCreate(this);

    }

    public void button1(View view) {
        String ceshishuju = printerUtils.printerText(this, "ceshishuju");

        Toast.makeText(this, ceshishuju, Toast.LENGTH_SHORT).show();
    }

    public void button2(View view) {
        String ceshishuju = printerUtils.printerQCBitmap(this, "ceshishuju", "好的呀");

        Toast.makeText(this, ceshishuju, Toast.LENGTH_SHORT).show();
    }

    public void button3(View view) {

        newinitlization.startPhotoRecoSFZ(this);

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        printerUtils.onDestory(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        IdentityInfo identityInfo = IDCardUtils.onActivityResult(this, requestCode, resultCode, data);
        newinitlization.onActivityResult(this, requestCode, resultCode, data);

    }

    @Override
    public void onSucessRecoIDCARD(People people) {
        Log.i("MainActivity", people.toString());
    }
}
