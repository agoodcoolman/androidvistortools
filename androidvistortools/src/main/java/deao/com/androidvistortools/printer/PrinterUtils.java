package deao.com.androidvistortools.printer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.telpo.tps550.api.TelpoException;
import com.telpo.tps550.api.printer.ThermalPrinter;
import com.telpo.tps550.api.util.StringUtil;
import com.telpo.tps550.api.util.SystemUtil;

import java.lang.ref.WeakReference;

import deao.com.androidvistortools.R;

/**
 * @author Administrator
 *         Date 2016/10/19
 *         打印的工具类
 *         调用此方法的时候，记得调用onCrate与onDestory方法。
 */
public class PrinterUtils {
    public static final String LINE = "-----------------------------\n";
    private static final int NOPAPER = 3;
    private static final int LOWBATTERY = 4;
    private static final int PRINTVERSION = 5;
    private static final int PRINTBARCODE = 6;
    private static final int PRINTQRCODE = 7;
    private static final int PRINTPAPERWALK = 8;
    private static final int PRINTCONTENT = 9;
    private static final int PRINTERR = 11;
    private static final int OVERHEAT = 12;
    private static final int PRINTCONTENTDIMISS = 13;
    private static final int INIT = 14;

    private final int MAKER = 13;
    private final int PRINTPICTURE = 14;
    private final int EXECUTECOMMAND = 15;

    /**
     * 电量低的时候
     */
    private boolean LowBattery = false;
    /**
     * 打印机么有纸
     */
    private boolean nopaper = false;
    private final PrintHanler printHanler;
    private final BroadcastReceiver printReceive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
                int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, BatteryManager.BATTERY_STATUS_NOT_CHARGING);
                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 0);
                //TPS390 can not print,while in low battery,whether is charging or not charging
                if(SystemUtil.getDeviceType() == StringUtil.DeviceModelEnum.TPS390.ordinal()){
                    if (level * 5 <= scale) {
                        LowBattery = true;
                    } else {
                        LowBattery = false;
                    }
                }else {
                    if (status != BatteryManager.BATTERY_STATUS_CHARGING) {
                        if (level * 5 <= scale) {
                            LowBattery = true;
                        } else {
                            LowBattery = false;
                        }
                    } else {
                        LowBattery = false;
                    }
                }
            }
            //Only use for TPS550MTK devices
            else if (action.equals("android.intent.action.BATTERY_CAPACITY_EVENT")) {
                int status = intent.getIntExtra("action", 0);
                int level = intent.getIntExtra("level", 0);
                if(status == 0){
                    if(level < 1){
                        LowBattery = true;
                    }else {
                        LowBattery = false;
                    }
                }else {
                    LowBattery = false;
                }
            }
        }
    };

    /**
     * 初始化 打印库
     * @param activity
     */
    public PrinterUtils(final Activity activity) {
        printHanler = new PrintHanler(activity);
        printHanler.sendEmptyMessage(INIT);

        // 初始化检测是否有打印纸
        new Thread(new Runnable() {
            String printVersion;
            @Override
            public void run() {
                try {
                    ThermalPrinter.start(activity);
                    ThermalPrinter.reset();
                    printVersion = ThermalPrinter.getVersion();
                } catch (TelpoException e) {
                    e.printStackTrace();
                } finally {
                    if (printVersion != null) {
                        Message message = new Message();
                        message.what = PRINTVERSION;
                        message.arg1 = 1;

                        printHanler.sendMessage(message);
                    } else {
                        Message message = new Message();
                        message.what = PRINTVERSION;
                        message.arg1 = 0;
                        message.obj = "0";
                        printHanler.sendMessage(message);
                    }
                    ThermalPrinter.stop(activity);
                }
            }
        }).start();
    }

    /**
     * 连接拦截电量低的警告广播
     */
    public void onCreate(Context context) {
        IntentFilter pIntentFilter = new IntentFilter();
        pIntentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        pIntentFilter.addAction("android.intent.action.BATTERY_CAPACITY_EVENT");
        context.registerReceiver(printReceive, pIntentFilter);
    }

    /**
     * 注销电量广播
     * @param context
     */
    public void onDestory(Context context) {
        context.unregisterReceiver(printReceive);
    }

    public String printerText(Context context, String content) {
        return printerText(context, 0, 0, 4, 2, content);
    }

    public String printerQCBitmap(Context context, String content, String additionalContent) {
        return printerQCBitmap(context, 0, 0, 4, 2, content, additionalContent);
    }

    /**
     * 打印文字
     * @param marginLeft (0-255) 距离左侧
     * @param lineDistance (0-255) 行距
     * @param textGray (0-12) 字体灰度
     * @param fontSize (0-4) 字体大小
     * @param content
     * @return 打印信息返回
     */
    public String printerText(Context context, int marginLeft, int lineDistance, int textGray, int fontSize, String content) {
        String result = "";
        if (content == null || content.length() < 1) {
            result = context.getString(R.string.left_margin) + context.getString(R.string.lengthNotEnougth);
            return result;
        }

        if (lineDistance < 0 || lineDistance >= 255) {
            result = context.getString(R.string.outOfLine) + context.getString(R.string.lengthNotEnougth);
            return result;
        }

        if (fontSize <= 0 || fontSize >= 4) {
            result = context.getString(R.string.outOfFont) + context.getString(R.string.lengthNotEnougth);
            return result;
        }

        if (textGray <= 0 || textGray >= 12) {
            result = context.getString(R.string.outOfGray) + context.getString(R.string.lengthNotEnougth);
            return result;
        }

        if (marginLeft < 0 || marginLeft >= 255) {
            result = context.getString(R.string.outOfLeft) + context.getString(R.string.lengthNotEnougth);
            return result;
        }

        if (LowBattery) {
            if (context instanceof Activity)
                lowBattery((Activity)context);
            printHanler.sendMessage(printHanler.obtainMessage(LOWBATTERY, 1, 0, null));
            result = context.getString(R.string.LowBattery);
            return result;
        } else {
            if (!nopaper) {
                new contentPrintThread(context, marginLeft, lineDistance, textGray, fontSize, content).start();
            } else {
                result = context.getString(R.string.ptintInit);
                return result;
            }
        }
        return result;
    }

    /**
     * 打印二维码
     * @param context 上下文
     * @param marginLeft 左侧距离
     * @param lineDistance 行间距
     * @param textGray 字体灰度
     * @param fontSize 字体大小
     * @param content 二维码的内容，根据内容自动生成二维码
     * @param additionalContent 在二维码的下面的文字。
     * @return 返回打印状态
     */
    public String printerQCBitmap(Context context, int marginLeft, int lineDistance, int textGray, int fontSize, String content, String additionalContent) {
        String result = "";
        if (content == null || content.length() < 1) {
            result = context.getString(R.string.left_margin) + context.getString(R.string.lengthNotEnougth);
            return result;
        }

        if (lineDistance < 0 || lineDistance >= 255) {
            result = context.getString(R.string.outOfLine) + context.getString(R.string.lengthNotEnougth);
            return result;
        }

        if (fontSize <= 0 || fontSize >= 4) {
            result = context.getString(R.string.outOfFont) + context.getString(R.string.lengthNotEnougth);
            return result;
        }

        if (textGray <= 0 || textGray >= 12) {
            result = context.getString(R.string.outOfGray) + context.getString(R.string.lengthNotEnougth);
            return result;
        }

        if (marginLeft < 0 || marginLeft >= 255) {
            result = context.getString(R.string.outOfLeft) + context.getString(R.string.lengthNotEnougth);
            return result;
        }

        if (LowBattery) {
            if (context instanceof Activity)
                lowBattery((Activity)context);
            printHanler.sendMessage(printHanler.obtainMessage(LOWBATTERY, 1, 0, null));
            result = context.getString(R.string.LowBattery);
            return result;
        } else {
            if (!nopaper) {
                if (context instanceof Activity)
                new qrcodePrintThread((Activity)context, marginLeft, lineDistance, textGray, fontSize, content, additionalContent).start();
            } else {
                result = context.getString(R.string.ptintInit);
                return result;
            }
        }
        return result;
    }

    /**
     * 打印二维码
     */
    private class qrcodePrintThread extends Thread {
        private final String additionalContent;
        final Activity activity;
        final int marginLeft;
        final int lineDistance;
        final int textGray;
        final int wordFont;
        String content;
        String Result;
        /**
         *
         * @param act 上下文对象
         * @param marginLeft 左边距
         * @param lineDistance 文字间距
         * @param textGray 字体灰度
         * @param fontSize 字体大小
         * @param bitmapContent 二维码的内容
         * @param additionalContent 附加在二维码下面的信息
         */
        public qrcodePrintThread(Activity act, int marginLeft, int lineDistance, int textGray, int fontSize, String bitmapContent, String additionalContent) {
            this.activity = act;
            this.marginLeft = marginLeft;
            this.lineDistance = lineDistance;
            this.textGray = textGray;
            this.wordFont = fontSize;
            this.content = bitmapContent;
            this.additionalContent = additionalContent;
        }

        @Override
        public void run() {
            super.run();
            try {
                printHanler.sendMessage(printHanler.obtainMessage(PRINTCONTENT, 1, 0, null));
                ThermalPrinter.start(activity);
                ThermalPrinter.reset();
                ThermalPrinter.setGray(textGray);
                Bitmap bitmap = CreateCode(content, BarcodeFormat.QR_CODE, 256, 256);
                if(bitmap != null){
                    ThermalPrinter.printLogo(bitmap);
                }
                ThermalPrinter.addString(additionalContent);
                ThermalPrinter.printString();
                ThermalPrinter.walkPaper(100);
            } catch (Exception e) {
                e.printStackTrace();
                Result = e.toString();
                if (Result.equals("com.telpo.tps550.api.printer.NoPaperException")) {
                    nopaper = true;
                } else if (Result.equals("com.telpo.tps550.api.printer.OverHeatException")) {
                    printHanler.sendMessage(printHanler.obtainMessage(OVERHEAT, 1, 0, null));
                } else {
                    printHanler.sendMessage(printHanler.obtainMessage(PRINTERR, 1, 0, null));
                }
            } finally {
                printHanler.sendMessage(printHanler.obtainMessage(PRINTCONTENTDIMISS, 1, 0, null));
                if (nopaper){
                    printHanler.sendMessage(printHanler.obtainMessage(NOPAPER, 1, 0, null));
                    nopaper = false;
                    return;
                }
                ThermalPrinter.stop(activity);
            }
        }
    }

    /**
     * 文字打印线程
     */
    private class contentPrintThread extends Thread {
        Context context;
        int marginLeft;
        int lineDistance;
        int textGray;
        int wordFont;
        String content;
        public contentPrintThread(Context con, int marginLeft, int lineDistance, int textGray, int fontSize, String content) {
            context = con;
            this.marginLeft = marginLeft;
            this.lineDistance = lineDistance;
            this.textGray = textGray;
            this.wordFont = fontSize;
            this.content = content;
        }

        @Override
        public void run() {
            super.run();
            try {

                printHanler.sendMessage(printHanler.obtainMessage(PRINTCONTENT, 1, 0, null));
                ThermalPrinter.start(context);
                ThermalPrinter.reset();
                ThermalPrinter.setAlgin(ThermalPrinter.ALGIN_LEFT);
                ThermalPrinter.setLeftIndent(marginLeft);
                ThermalPrinter.setLineSpace(lineDistance);
                if (wordFont == 4) {
                    ThermalPrinter.setFontSize(2);
                    ThermalPrinter.enlargeFontSize(2, 2);
                } else if (wordFont == 3) {
                    ThermalPrinter.setFontSize(1);
                    ThermalPrinter.enlargeFontSize(2, 2);
                } else if (wordFont == 2) {
                    ThermalPrinter.setFontSize(2);
                } else if (wordFont == 1) {
                    ThermalPrinter.setFontSize(1);
                }
                ThermalPrinter.setGray(textGray);
                ThermalPrinter.addString(content);
                ThermalPrinter.printString();
                ThermalPrinter.walkPaper(100);
            } catch (Exception e) {
                e.printStackTrace();
                String Result = e.toString();
                if (Result.equals("com.telpo.tps550.api.printer.NoPaperException")) {
                    nopaper = true;
                } else if (Result.equals("com.telpo.tps550.api.printer.OverHeatException")) {
                    printHanler.sendMessage(printHanler.obtainMessage(OVERHEAT, 1, 0, null));
                } else {
                    printHanler.sendMessage(printHanler.obtainMessage(PRINTERR, 1, 0, null));
                }
            } finally {
                if (!((Activity)context).isFinishing()) {

                    printHanler.sendMessage(printHanler.obtainMessage(PRINTCONTENTDIMISS, 1, 0, null));
                }
                if (nopaper){
                    printHanler.sendMessage(printHanler.obtainMessage(NOPAPER, 1, 0, null));
                    nopaper = false;
                    return;
                }
                ThermalPrinter.stop(context);
            }
        }
    }

    /**
     *
     * @param walkLines 走纸行数
     * @return
     */
    public String walkLines(int walkLines) {

        return "";
    }

    /**
     * 电量低的时候执行
     */
    private void lowBattery(Activity activity) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        alertDialog.setTitle(R.string.operation_result);
        alertDialog.setMessage(activity.getString(R.string.LowBattery));
        alertDialog.setPositiveButton(activity.getString(R.string.dialog_comfirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        alertDialog.show();
    }


    /**
     * 生成条码
     *
     * @param str
     *            条码内容
     * @param type
     *            条码类型： AZTEC, CODABAR, CODE_39, CODE_93, CODE_128, DATA_MATRIX,
     *            EAN_8, EAN_13, ITF, MAXICODE, PDF_417, QR_CODE, RSS_14,
     *            RSS_EXPANDED, UPC_A, UPC_E, UPC_EAN_EXTENSION;
     * @param bmpWidth
     *            生成位图宽,宽不能大于384，不然大于打印纸宽度
     * @param bmpHeight
     *            生成位图高，8的倍数
     */
    public Bitmap CreateCode(String str, com.google.zxing.BarcodeFormat type, int bmpWidth, int bmpHeight) throws WriterException {
        // 生成二维矩阵,编码时要指定大小,不要生成了图片以后再进行缩放,以防模糊导致识别失败
        BitMatrix matrix = new MultiFormatWriter().encode(str, type, bmpWidth, bmpHeight);
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        // 二维矩阵转为一维像素数组（一直横着排）
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (matrix.get(x, y)) {
                    pixels[y * width + x] = 0xff000000;
                } else {
                    pixels[y * width + x] = 0xffffffff;
                }
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        // 通过像素数组生成bitmap,具体参考api
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    static class PrintHanler extends Handler {
        WeakReference<Activity> weakReference;
        private ProgressDialog printProgressDialog;
        private ProgressDialog initProcess;

        public PrintHanler(Activity activity) {
            weakReference = new WeakReference<Activity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            final Activity activity = weakReference.get();
            switch (msg.what) {
                case NOPAPER:
                    AlertDialog.Builder dlg = new AlertDialog.Builder(activity);
                    dlg.setTitle(activity.getString(R.string.noPaper));
                    dlg.setMessage(activity.getString(R.string.noPaperNotice));
                    dlg.setCancelable(false);
                    dlg.setPositiveButton(R.string.sure, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ThermalPrinter.stop(activity);
                        }
                    });
                    dlg.show();
                    break;
                case LOWBATTERY:
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
                    alertDialog.setTitle(R.string.operation_result);
                    alertDialog.setMessage(activity.getString(R.string.LowBattery));
                    alertDialog.setPositiveButton(activity.getString(R.string.dialog_comfirm), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    alertDialog.show();
                    break;
                case PRINTCONTENT: // 打印内容的提示窗
                    printProgressDialog = ProgressDialog.show(activity, activity.getString(R.string.bl_dy), activity.getString(R.string.printing_wait));
                    break;
                case PRINTCONTENTDIMISS: // 打印内容提示窗关闭
                    if (printProgressDialog != null)
                    printProgressDialog.dismiss();
                    break;
                case PRINTVERSION: // 进来的时候进行初始化检测 检测版本
                    initProcess.dismiss();
                    if (msg.arg1 == 1) {

                    } else {
                        Toast.makeText(activity, R.string.operation_fail, Toast.LENGTH_LONG).show();
                    }
                    break;
                case INIT: // 进来的时候进行初始化检测 检测版本
                    initProcess = ProgressDialog.show(activity, activity.getString(R.string.idcard_czz), activity.getString(R.string.watting));

                    break;


            }
        }
    }
}
