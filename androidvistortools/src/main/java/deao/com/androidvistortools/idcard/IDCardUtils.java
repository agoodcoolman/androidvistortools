package deao.com.androidvistortools.idcard;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.telpo.tps550.api.idcard.IdentityInfo;

import deao.com.androidvistortools.R;

/**
 * @author Administrator
 *         Date 2016/10/19
 *         身份证识别的工具类
 *
 *         在想调用的地方直接调用 startRecoIDCardPostviceActivityd 这个方法
 *         然后重写onActivityResultd 这个方法获取到回调内容
 */
public class IDCardUtils {
    public final static int IDCARD_FRONT = 0x00101;
    public final static int IDCARD_BACK = 0x00102;
    public static final String TAG = "IDCardUtils";
    /**
     * 开始识别身份证 识别正面
     * @param headFile 身份证的头像
     * @param sfzFile 身份证的照片
     */
    public static void startRecoIDCardPostviceActivity(Activity context, String headFile, String sfzFile) {

        Intent intent = new Intent();
        intent.setClassName("com.telpo.tps550.api",
                "com.telpo.tps550.api.ocr.IdCardOcr");
        intent.putExtra("type", true);
        intent.putExtra("isKeepPicture", true);// 是否保存图片
        // true是，false:否，不传入时，默认为否
        if (sfzFile != null)
        intent.putExtra("PictPath", sfzFile);// 图片路径，不传入时保存到默认路径/sdcard/OCRPict
        intent.putExtra("PictFormat", "PNG");// 图片格式：JPEG，PNG，WEBP，不传入时默认为PNG格式
        if (headFile != null)
        intent.putExtra("show_head_photo", headFile);
        try {
            context.startActivityForResult(intent, IDCARD_FRONT);
        } catch (ActivityNotFoundException exception) {
            // "未安装API模块，无法进行二维码/身份证识别"
            /*Toast.makeText(OcrIdCardActivity.this,
                    getResources().getString(R.string.identify_fail),
                    Toast.LENGTH_LONG).show();*/
        }

    }

    /**
     * 开始识别身份证 识别反面
     * @param context 上下文对象
     * @param sfzFile
     */
    public static void startRecoIDCardBackActivity(Activity context,  String sfzFile) {

        Intent intent = new Intent();
        intent.setClassName("com.telpo.tps550.api",
                "com.telpo.tps550.api.ocr.IdCardOcr");
        intent.putExtra("type", false);
        intent.putExtra("isKeepPicture", true);// 是否保存图片
        // true是，false:否，不传入时，默认为否
        if (sfzFile != null)
            intent.putExtra("PictPath", sfzFile);// 图片路径，不传入时保存到默认路径/sdcard/OCRPict
        intent.putExtra("PictFormat", "PNG");// 图片格式：JPEG，PNG，WEBP，不传入时默认为PNG格式

        try {
            context.startActivityForResult(intent, IDCARD_BACK);
        } catch (ActivityNotFoundException exception) {
            // "未安装API模块，无法进行二维码/身份证识别"
            /*Toast.makeText(OcrIdCardActivity.this,
                    getResources().getString(R.string.identify_fail),
                    Toast.LENGTH_LONG).show();*/
        }
    }

    /**
     *
     * @param context 这都是系统封装的
     * @param requestCode 这都是系统封装的
     * @param resultCode 这都是系统封装的
     * @param data 这都是系统封装的
     * @return 返回身份证的对象。
     */
    public static IdentityInfo onActivityResult(Context context, int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case IDCARD_FRONT:
                if (resultCode == 0) {
                    IdentityInfo info = null;
                    try {
                        info = (IdentityInfo) data.getSerializableExtra("idInfo");
                    } catch (NullPointerException e) {
                        Log.i(TAG, context.getResources().getString(R.string.get_id_front_fail));
                        break;
                    }
                    if (info != null && info.getName() != null
                            && info.getNo() != null) {
                        Log.i(TAG, context.getResources().getString(R.string.get_id_front_success));
                        // 识别成功，返回正确的数据
                        return info;
                    } else {
                        Log.i(TAG, context.getResources().getString(R.string.get_id_front_fail));

                    }
                } else {
                    Log.i(TAG, context.getResources().getString(R.string.get_id_front_fail));

                }
                break;
            case IDCARD_BACK:
                if (resultCode == 0) {
                    IdentityInfo info = null;
                    try {
                        info = (IdentityInfo) data.getSerializableExtra("idInfo");
                    } catch (NullPointerException e) {
                        Log.i(TAG, context.getResources().getString(R.string.get_id_back_fail));

                        break;
                    }

                    if (info != null && info.getPeriod() != null
                            && info.getApartment() != null) {
                        Log.i(TAG, context.getResources().getString(R.string.get_id_back_success));

                        return info;
                    } else {
                        Log.i(TAG, context.getResources().getString(R.string.get_id_back_fail));

                    }
                } else {
                    Log.i(TAG, context.getResources().getString(R.string.get_id_back_fail));

                }
                break;

        }
        return null;
    }

}
